from time import sleep
from button import BUTTON
from machine import Pin
 
led = Pin(2,Pin.OUT)
btn = BUTTON(4,btnDeal,longCallBack=longBtnDeal) 
    
    
def btnDeal():
    led.value(1-led.value())

def longBtnDeal():
    for i in range(6):
        sleep(0.5)
        led.value(1-led.value())
    
while True:
    btn.cycle()
    sleep(0.01)
