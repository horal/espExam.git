'''
    @作者:史久超
    @日期:2021.11.13
    @描述:
    @历史:
'''
from machine import Pin

class BUTTON:
    def __init__(self,\
                 pin,\
                 pressCallBack,\
                 pressed_valve=3,\
                 long_valve=6,\
                 holding_valve=9,\
                 longCallBack=None,\
                 holdCallBack=None):
        self.btn = Pin(pin,Pin.IN)
        self.pressCallBack = pressCallBack
        self.pressed_valve = pressed_valve
        self.long_valve = long_valve
        self.holding_valve = holding_valve
        self.counter = 0
        self.holding_counter = 0
        
    def cycle(self):
        if self.btn.value():
            if self.counter < self.holding_valve:
                self.counter = self.counter + 1
            else:
                if self.holding_counter < self.pressed_valve - 2:
                    self.holding_counter = self.holding_counter + 1
                else:
                    self.holding_counter = 0
                    self.holdCallBack()         
        else:
            self.counter = 0
            self.holding_counter = 0
        
        if self.counter == self.pressed_valve:
            self.pressCallBack()
        elif self.counter == self.long_valve:
            self.longCallBack()
        else:
            pass

def demo():
    print("demo")
    

if __name__=="__main__":
    demo()