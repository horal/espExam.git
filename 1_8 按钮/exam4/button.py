'''
    @作者:史久超
    @日期:2021.11.13
    @描述:
    @历史:
'''
from machine import Pin

class BUTTON:
    def __init__(self,\
                 pin,\
                 pressCallBack,\
                 pressed_valve=3,\
                 long_valve=50,\
                 holding_valve=99,\
                 longCallBack=None,\
                 holdCallBack=None):
        self.btn = Pin(pin,Pin.IN)
        self.pressCallBack = pressCallBack
        self.longCallBack = longCallBack
        self.holdCallBack = holdCallBack
        self.pressed_valve = pressed_valve
        self.long_valve = long_valve
        self.holding_valve = holding_valve
        self.counter = 0
        self.holding_counter = 0
        self.current_state = "invalid"
        
    def cycle(self):
        if self.btn.value():
            if self.counter < self.holding_valve:
                self.counter = self.counter + 1
            else:
                if self.holding_counter < self.pressed_valve*10:
                    self.holding_counter = self.holding_counter + 1
                else:
                    self.holding_counter = 0
                    self.holdCallBack()
        else:
            if self.current_state == "down":
                self.pressCallBack()
            elif (self.current_state == "long") and (self.counter < self.holding_valve):
                self.longCallBack()
            else:
                pass
            self.counter = 0
            self.holding_counter = 0
            self.current_state = "invalid"
            
        if self.counter == self.pressed_valve:
            self.current_state="down"
        elif self.counter == self.long_valve:
            self.current_state="long"
        else:
            pass
        
def demo():
    print("demo")
    

if __name__=="__main__":
    demo()