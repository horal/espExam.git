from machine import Pin

#共阳单位数码管
class digitalCube1:
    '''
        类的接口函数,输入参数段码IO口.
    '''
    def __init__(self,a,b,c,d,e,f,g,dp):
        self.duan_ctl  = [Pin(dp,Pin.OUT),\
                          Pin(g,Pin.OUT),\
                          Pin(f,Pin.OUT),\
                          Pin(e,Pin.OUT),\
                          Pin(d,Pin.OUT),\
                          Pin(c,Pin.OUT),\
                          Pin(b,Pin.OUT),\
                          Pin(a,Pin.OUT)]
        self.duan_code = [0x03,0x9f,0x25,0x0d,0x99,0x49,0x41,0x1f,0x01,0x09]
        self.disp_buff = 0b111111
        
    '''
        更新要显示的内容value
    '''
    def updateDisplay(self,value):
        self.disp_buff = self.duan_code[value]
    
    '''
        显示函数,在时间片中调用
    '''
    def display(self):
        for i in range(8):
            if (self.disp_buff & (1<<i)):
                self.duan_ctl[i].on()
            else:
                self.duan_ctl[i].off()

#共阳双位数码管
class digitalCube2:
    '''
        类的接口函数,输入参数段码IO口.
    '''
    def __init__(self,a,b,c,d,e,f,g,dp,ge,shi):
        self.duan_ctl  = [Pin(dp,Pin.OUT),\
                          Pin(g,Pin.OUT),\
                          Pin(f,Pin.OUT),\
                          Pin(e,Pin.OUT),\
                          Pin(d,Pin.OUT),\
                          Pin(c,Pin.OUT),\
                          Pin(b,Pin.OUT),\
                          Pin(a,Pin.OUT),\
                          Pin(dp,Pin.OUT),\
                          Pin(g,Pin.OUT),\
                          Pin(f,Pin.OUT),\
                          Pin(e,Pin.OUT),\
                          Pin(d,Pin.OUT),\
                          Pin(c,Pin.OUT),\
                          Pin(b,Pin.OUT),\
                          Pin(a,Pin.OUT)]
        
        self.ge_ctl  =  Pin(ge,Pin.OUT)
        self.shi_ctl =  Pin(shi,Pin.OUT)
        self.duan_code = [0x03,0x9f,0x25,0x0d,0x99,0x49,0x41,0x1f,0x01,0x09]
        self.disp_buff = 0b111111
        self.index = 0
        
    '''
        更新要显示的内容value
    '''
    def updateDisplay(self,value):
        temp_shi = int(value/10)
        temp_ge  = (value%10)
        self.disp_buff = (0x00ff|(self.duan_code[temp_shi]<<8))&(0xff00|self.duan_code[temp_ge])
    
    '''
        显示函数,在时间片中调用
    '''
    def display(self):
        #关闭所有段
        for i in range(8):
            self.duan_ctl[i].on()
            
        if(self.disp_buff&(1<<self.index)):
            self.duan_ctl[self.index].on()
        else:
            self.duan_ctl[self.index].off()
            
        if(self.index > 7):
            self.shi_ctl.on()
            self.ge_ctl.off()
        else:
            self.shi_ctl.off()
            self.ge_ctl.on()       
        
        self.index = self.index + 1
        if self.index > 15:
            self.index = 0