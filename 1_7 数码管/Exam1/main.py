from machine import Pin
from time import sleep

#实例化段码
a  = Pin(15,Pin.OUT)
b  = Pin(4,Pin.OUT)
c  = Pin(5,Pin.OUT)
d  = Pin(18,Pin.OUT)
e  = Pin(19,Pin.OUT)
f  = Pin(21,Pin.OUT)
g  = Pin(22,Pin.OUT)
dp = Pin(23,Pin.OUT)

#关闭段码
a.on()
b.on()
c.on()
d.on()
e.on()
f.on()
g.on()
dp.on()

#段码
duan_ma  = [0x03,0x9f,0x25,0x0d,0x99,0x49,0x41,0x1f,0x01,0x09]
duan_ctl = [dp,g,f,e,d,c,b,a]

#显存
display_buff = 0b11111111

#显示函数
def display():
    global display_buff
    for j in range(8):
        if(display_buff&(1<<j)):
            duan_ctl[j].on()
        else:
            duan_ctl[j].off()
#循环计数器
i = 0

while True:
    display_buff = duan_ma[i]
    
    '''
    i = i+1
    if(i>9):
        i=0
    '''
    
    display(i%8)
    sleep(0.01)
