from machine import Pin
from time import sleep,sleep_ms

#实例化段码
a  = Pin(15,Pin.OUT)
b  = Pin(4,Pin.OUT)
c  = Pin(5,Pin.OUT)
d  = Pin(18,Pin.OUT)
e  = Pin(19,Pin.OUT)
f  = Pin(21,Pin.OUT)
g  = Pin(22,Pin.OUT)
dp = Pin(23,Pin.OUT)
gao = Pin(25,Pin.OUT)
di = Pin(33,Pin.OUT)

#关闭段码
a.on()
b.on()
c.on()
d.on()
e.on()
f.on()
g.on()
dp.on()
gao.on()
di.on()


#段码
duan_ma  = [0x03,0x9f,0x25,0x0d,0x99,0x49,0x41,0x1f,0x01,0x09]
duan_ctl = [dp,g,f,e,d,c,b,a,dp,g,f,e,d,c,b,a]

#显存
display_buff = 0b1111111111111111

#显示函数
def display(index):
    global display_buff
    for i in range(8):
        duan_ctl[i].on()
        
    if(index > 7):
        gao.on()
        di.off()
    else:
        gao.off()
        di.on()
        
    if(display_buff&(1<<index)):
        duan_ctl[index].on()
    else:
        duan_ctl[index].off() 

#更新显存
def updateDisplayBuff(value):
    global display_buff
    temp_shi = int(value/10)
    temp_ge  = (value%10)
    display_buff = (0x00ff|(duan_ma[temp_shi]<<8))&(0xff00|duan_ma[temp_ge])
    print(display_buff)
    
    
#循环计数器
i = 0
j = 0

while True:
    i = i+1
    if(i>999):
        i=0
        j = j+1
        updateDisplayBuff(j)
        #display_buff = 0x2503
        if(j>99):
            j=0
            
    display(i%16)
    sleep(0.001)

