from time import sleep
from machine import Pin

a = Pin(15,Pin.OUT)
b = Pin(4,Pin.OUT)
c = Pin(5,Pin.OUT)
d = Pin(18,Pin.OUT)
e = Pin(19,Pin.OUT)
f = Pin(21,Pin.OUT)
g = Pin(22,Pin.OUT)
dp = Pin(23,Pin.OUT)

ctl = Pin(33,Pin.OUT)
ctl.on()

code_array = [0b00000011,\
              0b10011111,\
              0b00100101,\
              0b00001101,\
              0b10011001,\
              0b01001001,\
              0b01000001,\
              0b00011111,\
              0b00000001,\
              0b00001001]

code_led = [dp,g,f,e,d,c,b,a]

def display(value,index):
    buff = code_array[value]
    
    j = 0
    while j<8:
        code_led[j].on()
        
    if (buff &(1<<index)):
        code_led[index].on()
    else:
        code_led[index].off()
    
    #print(j)    
    #print(value)
    

i = 0
j = 0

while True:
    display(j,i%8)
    i = i+1
    if i >999:
        i=0
        j = j+1
        if j>9:
            j=0
    sleep(0.001)