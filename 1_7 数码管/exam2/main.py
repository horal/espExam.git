from time import sleep
from machine import Pin

a = Pin(15,Pin.OUT)
b = Pin(4,Pin.OUT)
c = Pin(5,Pin.OUT)
d = Pin(18,Pin.OUT)
e = Pin(19,Pin.OUT)
f = Pin(21,Pin.OUT)
g = Pin(22,Pin.OUT)
dp = Pin(23,Pin.OUT)

ctl = Pin(33,Pin.OUT)
ctl.on()

code_array = [0b00000011,\
              0b10011111,\
              0b00100101,\
              0b00001101,\
              0b10011001,\
              0b01001001,\
              0b01000001,\
              0b00011111,\
              0b00000001,\
              0b00001001]



def display(value):
    buff = code_array[value]
    
    if buff & 0b1000000:
        a.on()
    else:
        a.off
        
    if buff & 0b01000000:
        b.on()
    else:
        b.off()
        
    if buff & 0b00100000:
        c.on()
    else:
        c.off()
        
    if buff & 0b00010000:
        d.on()
    else:
        d.off()
    
    if buff & 0b00001000:
        e.on()
    else:
        e.off()
    
    if buff & 0b00000100:
        f.on()
    else:
        f.off()
    
    if buff & 0b00000010:
        g.on()
    else:
        g.off()
    
    if buff & 0b0000000001:
        dp.on()
    else:
        dp.off()
    
        
    print(value)
    

i = 0
while True:
    display(i)
    i = i+1
    if i >9:
        i=0
    sleep(1)