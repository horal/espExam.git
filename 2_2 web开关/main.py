from machine import Pin
from microWebSrv import MicroWebSrv
import network
import json

config_dict = {}
led = Pin(2,Pin.OUT)
switch=Pin(27,Pin.OUT)

#读取配置信息
try:
    with open("config.json","r") as f:
        config_dict = json.load(f)
except:
    print("\tinit config file")
    config_dict = {"essid":"Controller","password":"12345678"}
    with open("config.json","w") as f:
        json.dump(config_dict,f)
#print("\t"+config_dict["essid"])
    

#配置wifi热点
ap = network.WLAN(network.AP_IF)
ap.config(essid=config_dict["essid"],authmode=4,password=config_dict["password"])
ap.active(True)
print("可通过ip:%s登录" %(ap.ifconfig()[0]))


def switchOn():
    led.on()
    switch.on()


def switchOff():
    led.off()
    switch.off()
    
        
@MicroWebSrv.route("\modify")
def _modifyHander(httpClient,httpResponse):
    #msg = httpClient.GetRequestQueryParams()
    #print(msg)
    global config_dict
    try:
        config_dict = httpClient.GetRequestQueryParams()
        if ("essid" in config_dict) and ("password" in config_dict):
            with open("config.json","w") as f:
                json.dump(config_dict,f)
    except:
        pass
    #print(httpClient.GetRequestQueryParams())
    content = """\
    <!DOCTYPE html>
    <html lang=en>
        <head>
            <meta charset="UTF-8" />
            <title>TEST GET</title>
        </head>
        <body>
            <center>
            <form name="input" action="modify" method="get">
                wifi id:<input type="text" value=%s name="essid">
                <br />
                password:<input value=%s type="password" name="password">
                <br />
                <input type="submit" value="Submit">
            </form>
            </center>
        </body>
    </html>
    """ %(config_dict["essid"],config_dict["password"])
    httpResponse.WriteResponseOk(headers = None,\
                                  contentType = "text/html",\
                                  contentCharset = "UTF-8",\
                                  content  = content )

@MicroWebSrv.route("")
def _LEDONHander(httpClient, httpResponse):
    if led.value():
        content = """\
        <!DOCTYPE html>
        <html lang=en>
            <head>
                <meta charset="UTF-8" />
                <title>TEST GET</title>
            </head>
            <body>
                <h1>wifi Switch</h1>
                <br />
                <center>
                    <a href="on"><b>ON</b></a>
                    <br />
                    <a href="off">OFF</a>
                </center>
                <hr>
                Client IP address = %s
            </body>
        </html>
        """ % httpClient.GetIPAddr()
        httpResponse.WriteResponseOk( headers = None,\
                                      contentType = "text/html",\
                                      contentCharset = "UTF-8",\
                                      content  = content )
        switchOn()
    else:
        content = """\
        <!DOCTYPE html>
        <html lang=en>
            <head>
                <meta charset="UTF-8" />
                <title>TEST GET</title>
            </head>
            <body>
                <h1>wifi Switch</h1>
                <br />
                <center>
                    <a href="on">ON</a>
                    <br />
                    <a href="off"><b>OFF</b></a>
                </center>
                <hr>
                Client IP address = %s
            </body>
        </html>
        """ % httpClient.GetIPAddr()
        httpResponse.WriteResponseOk( headers = None,\
                                      contentType = "text/html",\
                                      contentCharset = "UTF-8",\
                                      content  = content )
        switchOff()        
    
    
@MicroWebSrv.route("/on")
def _LEDONHander(httpClient, httpResponse):
    content = """\
    <!DOCTYPE html>
    <html lang=en>
        <head>
            <meta charset="UTF-8" />
            <title>TEST GET</title>
        </head>
        <body>
            <h1>wifi Switch</h1>
            <br />
            <center>
                <a href="on"><b>ON</b></a>
                <br />
                <a href="off">OFF</a>
            </center>
            <hr>
            Client IP address = %s
        </body>
    </html>
    """ % httpClient.GetIPAddr()
    httpResponse.WriteResponseOk( headers = None,\
                                  contentType = "text/html",\
                                  contentCharset = "UTF-8",\
                                  content  = content )
    switchOn()
    
    
@MicroWebSrv.route("/off")
def _LEDONHander(httpClient, httpResponse):
    content = """\
    <!DOCTYPE html>
    <html lang=en>
        <head>
            <meta charset="UTF-8" />
            <title>TEST GET</title>
        </head>
        <body>
            <h1>wifi Switch</h1>
            
            <br />
            <center>
                <a href="on">ON</a>
                <br />
                <a href="off"><b>OFF</b></a>
            </center>
            <hr>
            Client IP address = %s
        </body>
    </html>
    """ % httpClient.GetIPAddr()
    httpResponse.WriteResponseOk( headers = None,\
                                  contentType = "text/html",\
                                  contentCharset = "UTF-8",\
                                  content  = content )
    switchOff()
    
      
srv = MicroWebSrv(webPath='www/')
srv.Start()