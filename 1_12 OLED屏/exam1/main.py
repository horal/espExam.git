from ssd1306 import SSD1306_I2C
from machine import I2C
from time import sleep

display = SSD1306_I2C(128,64,I2C(1))

display.fill(0)
display.show()

temp = 9

while True:
    display.fill(0)
    display.text(str(temp),60,20)
    display.show()
    if (temp >0):
        temp -=1
    else:
        temp = 9
    sleep(1)

'''
    在屏幕上循环打印9~0倒计时

'''