EESchema Schematic File Version 4
LIBS:wanqu-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Module:ESP32-WROOM-32 U1
U 1 1 61A6D8C3
P 3150 4050
F 0 "U1" H 3450 5500 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 3550 5400 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 3150 2550 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 2850 4100 50  0001 C CNN
	1    3150 4050
	1    0    0    -1  
$EndComp
Text Notes 2050 1350 0    50   ~ 0
1.uart1's IO port:Tx->18/Rx->19(default IO 9/10 conficted with spiflash);\n2.screen ttl <-> uart1;\n3.stepper motor ttl <-> uart2;
$Comp
L power:+5V #PWR?
U 1 1 61A6F296
P 3150 2500
F 0 "#PWR?" H 3150 2350 50  0001 C CNN
F 1 "+5V" H 3165 2673 50  0000 C CNN
F 2 "" H 3150 2500 50  0001 C CNN
F 3 "" H 3150 2500 50  0001 C CNN
	1    3150 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2650 3150 2500
$Comp
L power:GND #PWR?
U 1 1 61A6FBDB
P 3150 5650
F 0 "#PWR?" H 3150 5400 50  0001 C CNN
F 1 "GND" H 3155 5477 50  0000 C CNN
F 2 "" H 3150 5650 50  0001 C CNN
F 3 "" H 3150 5650 50  0001 C CNN
	1    3150 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 5450 3150 5650
$Comp
L Interface_UART:MAX232 U2
U 1 1 61A7078B
P 6350 3750
F 0 "U2" H 6350 5131 50  0000 C CNN
F 1 "MAX232" H 6350 5040 50  0000 C CNN
F 2 "" H 6400 2700 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max232.pdf" H 6350 3850 50  0001 C CNN
	1    6350 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 61A8486E
P 4950 5450
F 0 "J1" H 4868 4825 50  0000 C CNN
F 1 "Conn_01x08" H 4868 4916 50  0000 C CNN
F 2 "" H 4950 5450 50  0001 C CNN
F 3 "~" H 4950 5450 50  0001 C CNN
	1    4950 5450
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 61A871B8
P 8200 4400
F 0 "J2" H 8118 3775 50  0000 C CNN
F 1 "Conn_01x08" H 8118 3866 50  0000 C CNN
F 2 "" H 8200 4400 50  0001 C CNN
F 3 "~" H 8200 4400 50  0001 C CNN
	1    8200 4400
	-1   0    0    1   
$EndComp
$EndSCHEMATC
