EESchema Schematic File Version 4
LIBS:schematic-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3900 2450 4150 2450
Wire Wire Line
	3900 2350 4150 2350
Wire Wire Line
	3900 1850 4150 1850
Wire Wire Line
	3900 1750 4150 1750
Wire Wire Line
	3900 1550 4150 1550
Wire Wire Line
	3900 1350 4150 1350
Wire Wire Line
	3900 1250 4150 1250
Text Label 4150 2550 0    50   ~ 0
D22
Text Label 4150 2650 0    50   ~ 0
D23
Text Label 4150 2450 0    50   ~ 0
TX0
Text Label 4150 2350 0    50   ~ 0
RX0
Text Label 4150 2250 0    50   ~ 0
D21
Text Label 4150 2150 0    50   ~ 0
D19
Text Label 4150 2050 0    50   ~ 0
D18
Text Label 4150 1950 0    50   ~ 0
D5
Text Label 4150 1850 0    50   ~ 0
TX2
Text Label 4150 1750 0    50   ~ 0
RX2
Text Label 4150 1650 0    50   ~ 0
D4
Text Label 4150 1550 0    50   ~ 0
D2
Text Label 4150 1450 0    50   ~ 0
D15
Text Label 4150 1350 0    50   ~ 0
GND
Text Label 4150 1250 0    50   ~ 0
3V3
$Comp
L Connector_Generic:Conn_02x15_Odd_Even ESP32_1
U 1 1 6182AFB2
P 3600 1950
F 0 "ESP32_1" H 3650 2775 50  0000 C CNN
F 1 "Conn_02x15_Odd_Even" H 3650 2776 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x15_P2.54mm_Vertical_SMD" H 3600 1950 50  0001 C CNN
F 3 "~" H 3600 1950 50  0001 C CNN
	1    3600 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R00
U 1 1 618B08AB
P 5100 1950
F 0 "R00" V 5100 1900 50  0001 L CNN
F 1 "R01" V 5100 1900 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5030 1950 50  0001 C CNN
F 3 "~" H 5100 1950 50  0001 C CNN
	1    5100 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 1950 5250 1950
Wire Wire Line
	5250 2150 5400 2150
Wire Wire Line
	5400 2350 5250 2350
Text Label 10650 2100 0    50   ~ 0
3V3
Wire Wire Line
	10550 2100 10650 2100
Wire Wire Line
	4950 1950 4700 1950
Wire Wire Line
	4700 1950 4700 2050
Wire Wire Line
	4700 2050 3900 2050
Wire Wire Line
	4950 2150 3900 2150
Wire Wire Line
	4950 2350 4700 2350
Wire Wire Line
	4700 2350 4700 2250
Wire Wire Line
	4700 2250 3900 2250
Wire Wire Line
	5800 2150 6050 2150
Text Label 6050 2150 0    50   ~ 0
GND
Wire Wire Line
	3900 1450 4150 1450
Wire Wire Line
	3900 1650 4150 1650
Wire Wire Line
	3900 1950 4150 1950
Wire Wire Line
	3900 2550 4150 2550
Wire Wire Line
	4150 2650 3900 2650
$Comp
L Device:R R?
U 1 1 618D3E17
P 5100 2150
F 0 "R?" V 5100 2100 50  0001 L CNN
F 1 "R02" V 5100 2100 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5030 2150 50  0001 C CNN
F 3 "~" H 5100 2150 50  0001 C CNN
	1    5100 2150
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 618D42BE
P 5100 2350
F 0 "R?" V 5100 2300 50  0001 L CNN
F 1 "R03" V 5100 2300 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5030 2350 50  0001 C CNN
F 3 "~" H 5100 2350 50  0001 C CNN
	1    5100 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 4550 4150 4550
Wire Wire Line
	3900 4450 4150 4450
Wire Wire Line
	3900 3950 4150 3950
Wire Wire Line
	3900 3850 4150 3850
Wire Wire Line
	3900 3650 4150 3650
Wire Wire Line
	3900 3450 4150 3450
Wire Wire Line
	3900 3350 4150 3350
Text Label 4150 4650 0    50   ~ 0
D22
Text Label 4150 4750 0    50   ~ 0
D23
Text Label 4150 4550 0    50   ~ 0
TX0
Text Label 4150 4450 0    50   ~ 0
RX0
Text Label 4150 4350 0    50   ~ 0
D21
Text Label 4150 4250 0    50   ~ 0
D19
Text Label 4150 4150 0    50   ~ 0
D18
Text Label 4150 4050 0    50   ~ 0
D5
Text Label 4150 3950 0    50   ~ 0
TX2
Text Label 4150 3850 0    50   ~ 0
RX2
Text Label 4150 3750 0    50   ~ 0
D4
Text Label 4150 3650 0    50   ~ 0
D2
Text Label 4150 3550 0    50   ~ 0
D15
Text Label 4150 3450 0    50   ~ 0
GND
Text Label 4150 3350 0    50   ~ 0
3V3
$Comp
L Connector_Generic:Conn_02x15_Odd_Even ESP32_2
U 1 1 618E8B7E
P 3600 4050
F 0 "ESP32_2" H 3650 4875 50  0000 C CNN
F 1 "Conn_02x15_Odd_Even" H 3650 4876 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x15_P2.54mm_Vertical_SMD" H 3600 4050 50  0001 C CNN
F 3 "~" H 3600 4050 50  0001 C CNN
	1    3600 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 618E8B84
P 5100 4050
F 0 "R?" V 5100 4000 50  0001 L CNN
F 1 "R01" V 5100 4000 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5030 4050 50  0001 C CNN
F 3 "~" H 5100 4050 50  0001 C CNN
	1    5100 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 4050 5250 4050
Wire Wire Line
	5250 4250 5400 4250
Wire Wire Line
	5400 4450 5250 4450
$Comp
L Device:LED_RGBA D2
U 1 1 618E8B8D
P 5600 4250
F 0 "D2" H 5600 4655 50  0000 C CNN
F 1 "LED_RGBA" H 5600 4656 50  0001 C CNN
F 2 "" H 5600 4200 50  0001 C CNN
F 3 "~" H 5600 4200 50  0001 C CNN
	1    5600 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4050 4700 4050
Wire Wire Line
	4700 4050 4700 4150
Wire Wire Line
	4700 4150 3900 4150
Wire Wire Line
	4950 4250 3900 4250
Wire Wire Line
	4950 4450 4700 4450
Wire Wire Line
	4700 4450 4700 4350
Wire Wire Line
	4700 4350 3900 4350
Wire Wire Line
	5800 4250 6050 4250
Text Label 6050 4250 0    50   ~ 0
3V3
Wire Wire Line
	3900 3550 4150 3550
Wire Wire Line
	3900 3750 4150 3750
Wire Wire Line
	3900 4050 4150 4050
Wire Wire Line
	3900 4650 4150 4650
Wire Wire Line
	4150 4750 3900 4750
$Comp
L Device:R R?
U 1 1 618E8BA1
P 5100 4250
F 0 "R?" V 5100 4200 50  0001 L CNN
F 1 "R02" V 5100 4200 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5030 4250 50  0001 C CNN
F 3 "~" H 5100 4250 50  0001 C CNN
	1    5100 4250
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 618E8BA7
P 5100 4450
F 0 "R?" V 5100 4400 50  0001 L CNN
F 1 "R03" V 5100 4400 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5030 4450 50  0001 C CNN
F 3 "~" H 5100 4450 50  0001 C CNN
	1    5100 4450
	0    1    1    0   
$EndComp
$Comp
L Device:LED_CRGB D1
U 1 1 618F6B3C
P 5600 2150
F 0 "D1" H 5600 1800 50  0000 C CNN
F 1 "LED_CRGB" H 5600 2556 50  0001 C CNN
F 2 "" H 5600 2100 50  0001 C CNN
F 3 "~" H 5600 2100 50  0001 C CNN
	1    5600 2150
	-1   0    0    1   
$EndComp
$EndSCHEMATC
