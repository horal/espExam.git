from machine import Pin,PWM
from time import sleep

#r/g/b实例
r = PWM(Pin(18,Pin.OUT),freq=100,duty=0)
g = PWM(Pin(19,Pin.OUT),freq=100,duty=0)
b = PWM(Pin(21,Pin.OUT),freq=100,duty=0)

#[红、绿、蓝]
color_ary = [[1023,0,0],[0,1023,0],[0,0,1023]]

#函数演示
def color(r,g,b):
    r.duty=color_ary[i][0]
    g.duty=color_ary[i][1]
    b.duty=color_ary[i][2]
    
#计数器
i = 0

#循环
while True:
    #配置各通道duty
    print("%d:r->%d,g-%d,b-%d" %(i,color_ary[i][0],color_ary[i][1],color_ary[i][2]))
    color(color_ary[i][0],color_ary[i][1],color_ary[i][2])
    
    #指向下一颜色
    i = i+1
    if(i>2):
        i = 0
    #延时1S
    sleep(1)