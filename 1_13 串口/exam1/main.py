from machine import UART
from time import sleep

uart2 = UART(2,115200)
uart2.init(115200)

print("system ready")

while True:
    rd = uart2.readline()
    if(rd!=None):
        print(rd)
    sleep(0.1)