from rfid import RFID
from time import sleep

module = RFID()

while True:
    module.waitMsg()
    ret,ic_type,ic_number = module.readID()
    if ret:
        print("Valid card{type:"+ic_type+";number:"+ic_number+"}")
    sleep(1)