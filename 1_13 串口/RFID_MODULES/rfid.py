from machine import UART
from time import sleep


class RFID:
    def __init__(self,port=1,baudrate=115200,rx=18,tx=19):
        self.port = UART(port,baudrate=baudrate,rx=tx,tx=tx)
        self.new_card_flag = False
        self.read_addr = 1
        self.read_buff = []
        self.card_msg = []
        self.block_index=[1,2,\
                          4,5,6,\
                          8,9,10,\
                          12,13,14,\
                          16,17,18,\
                          20,21,22,\
                          24,25,26,\
                          28,29,30,\
                          32,33,34,\
                          36,37,38,\
                          40,41,42,\
                          44,45,46,\
                          48,49,50,\
                          52,53,54,\
                          56,57,58,\
                          60,61,62]
        
    
    def waitMsg(self):
        dat = []
        if(self.port.any()):
            sleep(0.1)
            moreBytes = self.port.any()
            if moreBytes:
                dat = self.port.read()
                if (dat[0]==0x04) and (dat[2]==0x02):
                    print("valid card:",dat)
                    self.new_card_flag = True
                    for tdat in dat[5:11]:
                        self.card_msg.append(tdat)
                    self.readBlock(self.block_index[self.read_addr])
                    self.read_addr += 1
                elif (dat[0]==0x01) and (dat[2]==0xa3):
                    for i in range(16):
                        if dat[i+5] != 0:
                            self.read_buff.append(dat[i+5])
                        else:
                            self.readString()
                        break
                        if i == 15:
                            self.readBlock(self.block_index[self.read_addr])
                            self.read_addr += 1
        return(False,0)
    
    
    def readID(self):
        if(len(self.card_msg)):
            card_type = "{:02x}{:02x}".format(self.card_msg[0],self.card_msg[1])
            card_num = "{:02x}{:02x}{:02x}{:02x}".format(self.card_msg[2],self.card_msg[3],self.card_msg[4],self.card_msg[5])
            if self.new_card_flag:
                return(True,card_type,card_num)
                self.new_card_flag = False
            else:
                return(False,0,0)
        else:
            return(False,0,0)
     
    
    def readString(self):
        print("readed:",self.read_buff)
        temp = bytes(self.read_buff)
        print(temp)
        self.read_buff.clear()
        self.read_addr=0
    
    
    def _crcCalculate(dats):
        crc_temp = dats[0]
        for dat in dats[1:-1]:
            crc_temp ^= dat
        crc_temp = ~crc_temp
        return(crc_temp&0xff)
    
    
    def _readBlock(self,addr):
        cmd = [0x01,0x08,0xa3,0x20]
        cmd.append(addr&0xff)
        cmd.append(0x01)
        cmd.append(0x00)
        cmd.append(0x00)
        cmd[-1] = self._crcCalculate(cmd)
        self.port.write(cmd)
        