﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.IO;

namespace thread
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public int BaudRate;

        public byte[] RevDataBuffer = new byte[30];
        public UInt32 RevDataBufferCount;

        SerialPort Yserial = new SerialPort();

        private void Form1_Load(object sender, EventArgs e)
        {
            SearchPort();
        }
        public void SearchPort()
        {
            string[] ports = SerialPort.GetPortNames();

            foreach (string port in ports)
            {
                comboBox1.Items.Add(port);
            }

            if (ports.Length > 0)
            {
                comboBox1.Text = ports[0];
            }
            else
            {
                MessageBox.Show("没有发现可用端口");
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
          if (comboBox1.Items.Count != 0)
          {
              try
              {
                if (button2.Text == "打开端口")
                {
                  if (Yserial.IsOpen == false)
                  {
                    Yserial.PortName = comboBox1.Text;
                    Yserial.BaudRate = 9600;
                    Yserial.Open();
                    button2.Text = "关闭端口";
                    timer1.Enabled = true;
                    timer1.Start();
                  }
                }
                else
                {
                  if (Yserial.IsOpen)
                  {
                    Yserial.Close();
                  }
                  timer1.Enabled = false;
                  timer1.Stop();
                  button2.Text = "打开端口";
                }
              }
              catch//InvalidOperationException w
              {
                  MessageBox.Show("   端口被占用\n请选择正确的端口", "错误");
              }
          }
          else
          {
              MessageBox.Show("请选择正确的端口", "错误");
          }
        }
        private void Form1Close(object sender, FormClosedEventArgs e)
        {
          if (Yserial.IsOpen)
          {
            Yserial.Close();
          }
          this.Dispose();//关闭程序释放资源
        }

        public string byteToHexStr(byte[] bytes, int len)  //数组转十六进制字符显示
        {
            string returnStr = "";
            if (bytes != null)
            {
                for (int i = 0; i < len; i++)
                {
                    returnStr += bytes[i].ToString("X2");
                }
            }
            return returnStr;
        }

        public static bool CheckSumIn(byte[] buf, byte len)
        {
          byte i;
          byte checksum;
          checksum = 0;
          for (i = 0; i < (len - 1); i++)
          {
            checksum ^= buf[i];
          }
          if (buf[len - 1] == (byte)~checksum)
          {
            return true;
          }
          return false;
        }
        private void button1_Click(object sender, EventArgs e)
        {
          richTextBox1.AppendText("双击鼠标右键清除对话框内信息\n");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
          int revbuflen;

          byte pkttype;
          byte pktlength = 0x0;
          byte cmd;
          byte err;

          byte[] CardTpye = new byte[2];
          byte[] CardId = new byte[4];
          byte[] CardBlockData = new byte[16];

          bool revflag;
          bool status;


          byte[] rdatacopy = new byte[30];

          try
          {
            if (Yserial.IsOpen)
            {
              revbuflen = Yserial.BytesToRead;  //读缓冲区中数据长度
              revflag = false;
              if (revbuflen > 0)  //判断是否有数据
              {
                revflag = true;
                System.Threading.Thread.Sleep(50);
              }
              RevDataBufferCount = 0;
              while (revflag) //从串口缓冲区把数据读入到RevDataBufferCount数组中
              {
                RevDataBuffer[RevDataBufferCount] = (byte)Yserial.ReadByte();
                RevDataBufferCount = RevDataBufferCount + 1;
                if (RevDataBufferCount >= 30)//防止缓冲区溢出
                {
                  RevDataBufferCount = 0;
                }
                System.Threading.Thread.Sleep(2);
                revbuflen = Yserial.BytesToRead;
                if (revbuflen > 0)
                {
                  revflag = true;
                }
                else
                {
                  revflag = false;
                }
              }
              if ((RevDataBuffer[1] <= RevDataBufferCount) && (RevDataBufferCount != 0x0)) //判断是否接收到一帧数据
              {
                RevDataBufferCount = 0x0;
                status = CheckSumIn(RevDataBuffer, RevDataBuffer[1]);//计算校验和
                if (status == false)
                {
                  return;
                }
                pkttype = RevDataBuffer[0];  //获取包类型
                pktlength = RevDataBuffer[1]; //获取包长度
                cmd = RevDataBuffer[2]; //获取命令
                err = RevDataBuffer[4];//获取状态

                if (err != 0)
                {
                  return;
                }
                for (int i = 0; i < pktlength - 5; i++)//获取数据
                {
                  RevDataBuffer[i] = RevDataBuffer[i + 5]; 
                }
                if (pkttype == 0x04)
                {
                  switch (cmd)
                  {
                    case 0x02: //自动读卡号
                      for (int i = 0; i < 2; i++) //获取卡类型
                      { 
                        CardTpye[i] = RevDataBuffer[i];
                      }

                      for (int i = 0; i < 4; i++) //获取卡号
                      {
                        CardId[i] = RevDataBuffer[i + 2];
                      }
                      string strString = byteToHexStr(CardTpye, 2); 
                      richTextBox1.AppendText("卡类型：" + strString.ToString() + "\n");
                      strString = byteToHexStr(CardId, 4);
                      richTextBox1.AppendText("卡号：" + strString.ToString() + "\n"); 
                      break;
                    case 0x03: //自动读卡数据块
                      strString = byteToHexStr(RevDataBuffer, pktlength - 6);
                      richTextBox1.AppendText("块数据：" + strString + "\n"); 
                      break;
                    case 0x04: //自动读卡号 + 数据块
                      for (int i = 0; i < 2; i++) //获取卡类型
                      { 
                        CardTpye[i] = RevDataBuffer[i];
                      }
                      for (int i = 0; i < 4; i++) //获取卡号
                      {
                        CardId[i] = RevDataBuffer[i + 2];
                      }
                      for (int i = 0; i < 16; i++) //获取数据块数据
                      {
                        CardBlockData[i] = RevDataBuffer[i + 6];
                      }
                      strString = byteToHexStr(CardTpye, 2); 
                      richTextBox1.AppendText("卡类型：" + strString.ToString() + "\n");
                      strString = byteToHexStr(CardId, 4);
                      richTextBox1.AppendText("卡号：" + strString.ToString() + "\n");
                      strString = byteToHexStr(CardBlockData, 16);
                      richTextBox1.AppendText("块数据：" + strString.ToString() + "\n"); 
                      break;
                    default:
                      break;
                  }
                }
              }
            }
            else
            {
              button2.Text = "打开端口";
            }
          }
          catch
          {
            button2.Text = "打开端口";
            if (Yserial.IsOpen)
            {
              Yserial.Close();
            }
          }
        }
   }
}
