﻿namespace thread
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
          this.comboBox1 = new System.Windows.Forms.ComboBox();
          this.comboBox2 = new System.Windows.Forms.ComboBox();
          this.button2 = new System.Windows.Forms.Button();
          this.textBox1 = new System.Windows.Forms.TextBox();
          this.label1 = new System.Windows.Forms.Label();
          this.label5 = new System.Windows.Forms.Label();
          this.label6 = new System.Windows.Forms.Label();
          this.label7 = new System.Windows.Forms.Label();
          this.label8 = new System.Windows.Forms.Label();
          this.textBox2 = new System.Windows.Forms.TextBox();
          this.SuspendLayout();
          // 
          // comboBox1
          // 
          this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
          this.comboBox1.FormattingEnabled = true;
          this.comboBox1.Location = new System.Drawing.Point(99, 32);
          this.comboBox1.Name = "comboBox1";
          this.comboBox1.Size = new System.Drawing.Size(59, 20);
          this.comboBox1.TabIndex = 3;
          // 
          // comboBox2
          // 
          this.comboBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
          this.comboBox2.FormattingEnabled = true;
          this.comboBox2.Items.AddRange(new object[] {
            "4800",
            "7200",
            "9600",
            "14400",
            "19200",
            "38400",
            "57600",
            "115200"});
          this.comboBox2.Location = new System.Drawing.Point(211, 32);
          this.comboBox2.Name = "comboBox2";
          this.comboBox2.Size = new System.Drawing.Size(64, 20);
          this.comboBox2.TabIndex = 5;
          this.comboBox2.Text = "9600";
          this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
          // 
          // button2
          // 
          this.button2.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
          this.button2.Location = new System.Drawing.Point(149, 90);
          this.button2.Name = "button2";
          this.button2.Size = new System.Drawing.Size(172, 39);
          this.button2.TabIndex = 6;
          this.button2.Text = "开启运行";
          this.button2.UseVisualStyleBackColor = true;
          this.button2.Click += new System.EventHandler(this.button2_Click);
          // 
          // textBox1
          // 
          this.textBox1.Location = new System.Drawing.Point(364, 32);
          this.textBox1.Name = "textBox1";
          this.textBox1.Size = new System.Drawing.Size(60, 21);
          this.textBox1.TabIndex = 52;
          this.textBox1.Text = "20";
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(52, 35);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(41, 12);
          this.label1.TabIndex = 53;
          this.label1.Text = "端口号";
          // 
          // label5
          // 
          this.label5.AutoSize = true;
          this.label5.Location = new System.Drawing.Point(164, 35);
          this.label5.Name = "label5";
          this.label5.Size = new System.Drawing.Size(41, 12);
          this.label5.TabIndex = 54;
          this.label5.Text = "波特率";
          // 
          // label6
          // 
          this.label6.AutoSize = true;
          this.label6.Location = new System.Drawing.Point(291, 35);
          this.label6.Name = "label6";
          this.label6.Size = new System.Drawing.Size(53, 12);
          this.label6.TabIndex = 55;
          this.label6.Text = "设备地址";
          // 
          // label7
          // 
          this.label7.AutoSize = true;
          this.label7.Location = new System.Drawing.Point(166, 90);
          this.label7.Name = "label7";
          this.label7.Size = new System.Drawing.Size(0, 12);
          this.label7.TabIndex = 62;
          // 
          // label8
          // 
          this.label8.AutoSize = true;
          this.label8.Location = new System.Drawing.Point(523, 90);
          this.label8.Name = "label8";
          this.label8.Size = new System.Drawing.Size(0, 12);
          this.label8.TabIndex = 63;
          // 
          // textBox2
          // 
          this.textBox2.BackColor = System.Drawing.SystemColors.MenuBar;
          this.textBox2.ForeColor = System.Drawing.Color.Blue;
          this.textBox2.Location = new System.Drawing.Point(-2, 152);
          this.textBox2.Name = "textBox2";
          this.textBox2.Size = new System.Drawing.Size(489, 21);
          this.textBox2.TabIndex = 64;
          this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
          // 
          // Form1
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(485, 172);
          this.Controls.Add(this.textBox2);
          this.Controls.Add(this.label8);
          this.Controls.Add(this.label7);
          this.Controls.Add(this.label6);
          this.Controls.Add(this.label5);
          this.Controls.Add(this.label1);
          this.Controls.Add(this.textBox1);
          this.Controls.Add(this.button2);
          this.Controls.Add(this.comboBox1);
          this.Controls.Add(this.comboBox2);
          this.ForeColor = System.Drawing.Color.Black;
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
          this.MaximizeBox = false;
          this.Name = "Form1";
          this.Text = "RFID读写软件 V1.5  ";
          this.Load += new System.EventHandler(this.Form1_Load);
          this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1Close);
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox2;
    }
}

