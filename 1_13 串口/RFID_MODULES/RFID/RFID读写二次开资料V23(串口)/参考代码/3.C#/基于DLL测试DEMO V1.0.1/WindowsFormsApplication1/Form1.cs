﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using HFrfid;

namespace WindowsFormsApplication1
{
  public partial class Form1 : Form
  {

    public Form1()
    {
      InitializeComponent();
      
    }

    RfidReader Reader = new RfidReader();
   
    public void SearchPort()  //搜索端口
    {
      string[] ports = SerialPort.GetPortNames();
      foreach (string port in ports)
      {
        comboBox1.Items.Add(port);
      }

      if (ports.Length > 0)
      {
        comboBox1.Text = ports[0];
      }
      else
      {
        MessageBox.Show("没有发现可用端口");
      }
    }

    public string byteToHexStr(byte[] bytes, int len)  //数组转十六进制字符
    {
      string returnStr = "";
      if (bytes != null)
      {
        for (int i = 0; i < len; i++)
        {
          returnStr += bytes[i].ToString("X2");
        }
      }
      return returnStr;
    }

    private static byte[] strToToHexByte(string hexString) //字符串转16进制
    {
      //hexString = hexString.Replace(" ", " "); 
      if ((hexString.Length % 2) != 0)
        hexString = "0" + hexString;
      byte[] returnBytes = new byte[hexString.Length / 2];
      for (int i = 0; i < returnBytes.Length; i++)
        returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
      return returnBytes;
    }
    private void Form1_Load(object sender, EventArgs e)
    {
      comboBox1.Items.Clear();
      comboBox1.Text = null;
      SearchPort();
    }
    private void button3_Click(object sender, EventArgs e)
    {
      comboBox1.Items.Clear();
      comboBox1.Text = null;
      SearchPort();
      Reader.DisConnect();
      button2.Text = "打开端口";
    }

    private void button2_Click(object sender, EventArgs e)
    {
      try
      {
        if (button2.Text == "打开端口")
        {
          bool flg = Reader.Connect(comboBox1.Text, 9600);
          if (flg == true)
          {
            button2.Text = "关闭端口";
          }
          else
          {
            MessageBox.Show("端口无法打开");
          }
        }
        else
        {
          Reader.DisConnect();
          button2.Text = "打开端口";
        }
      }
      catch
      {
        textBoxStatus.Text = "端口被占用";
      }
    }
    private void button1_Click(object sender, EventArgs e)
    {
      int status;
      byte[] type = new byte[2];
      byte[] id = new byte[4];
      textBoxStatus.Clear();
      textBox5.Clear();
      textBox2.Clear();

      Reader.Cmd = Cmd.M1_ReadId;//读卡号命令
      Reader.Addr = Convert.ToByte(textBox1.Text, 16);//读写器地址,设备号
      Reader.Beep = Beep.On;

      status = Reader.M1_Operation();
      if (status == 0)//读卡成功
      {
        for (int i = 0; i < 2; i++)//获取2字节卡类型
        {
          type[i] = Reader.RxBuffer[i]; 
        }
        for (int i = 0; i < 04; i++)//获取4字节卡号
        {
          id[i] = Reader.RxBuffer[i + 2];
        }
        string ss = byteToHexStr(type, 2);
        textBox2.Text = ss;
        ss = byteToHexStr(id, 4);
        textBox5.Text = ss;
        textBoxStatus.Text = "读卡号成功";
      }
      else
      {
        textBoxStatus.Text = "错误码：" + status.ToString();
      }
    }
    private void button4_Click(object sender, EventArgs e)
    {
      int status;
      byte[] blockdata = new byte[16];
      textBoxStatus.Clear();
      textBox1_3.Clear();

      if (comboBox1_2.Text == "选择数据块")
      {
        textBoxStatus.Text = "请选择数据块";
        return;
      }

      if (comboBox1_1.Text == "KeyA")
        Reader.Cmd = Cmd.M1_KeyA_ReadBlock; //读卡数据块命令，验证KeyA
      else if (comboBox1_1.Text == "KeyB")
        Reader.Cmd = Cmd.M1_KeyB_ReadBlock; //读卡数据块命令，验证KeyB
      else
        return;

      Reader.Addr = Convert.ToByte(textBox1.Text, 16); //读写器地址
      Reader.M1Block = Convert.ToByte(comboBox1_2.Text, 10);//数据块块号
      Reader.Beep = Beep.On;//读数据块成功蜂鸣器提示

      status = Reader.M1_Operation();//读操作
      if (status == 0)//读成功
      {
        for (int i = 0; i < 16; i++)
        {
          blockdata[i] = Reader.RxBuffer[i];//获取读到的数据，将读到的数据拷贝到blockdata数组
        }
        string ss = byteToHexStr(blockdata, 16); //将读取的数据转换成字符串
        textBox1_3.Text = ss;
        textBoxStatus.Text = "读指定块数据成功";
      }
      else
      {
        textBoxStatus.Text = "错误码：" + status.ToString();//错误码参照DLL说明手册
      }
    }

    private void button5_Click(object sender, EventArgs e)
    {
      int status;

      byte[] buff = new byte[16];
      textBoxStatus.Clear();

      if (textBox2_3.Text == "")
      {
        textBoxStatus.Text = "写数据不能为空";
        return;
      }
      if (textBox2_3.Text.Length != 32)
      {
        textBoxStatus.Text = "写数据长度错误，请保证每次写入16字节数据";
        return;
      }
      byte[] data = strToToHexByte(textBox2_3.Text);

      if (comboBox2_2.Text == "选择数据块")
      {
        textBoxStatus.Text = "请选择数据块";
        return;
      }
    
      if (comboBox2_1.Text == "KeyA")
        Reader.Cmd = Cmd.M1_KeyA_WriteBlock;
      else if (comboBox2_1.Text == "KeyB")
        Reader.Cmd = Cmd.M1_KeyB_WriteBlock;
      else
        return;
      Reader.Addr = Convert.ToByte(textBox1.Text, 16);
      Reader.M1Block = Convert.ToByte(comboBox2_2.Text, 10);
      Reader.Beep = Beep.On;

      for (int i = 0; i < 16; i++)
        Reader.TxBuffer[i] = data[i];//将要写的16字节数保存到结构体成员TxBuffer数组中

      status = Reader.M1_Operation();
      if (status == 0x00)//写成功
      {
        textBoxStatus.Text = "写数据到指定块成功";
        return;
      }
      textBoxStatus.Text = "错误码：" + status.ToString();
    }

    private void button6_Click(object sender, EventArgs e)
    {
      int status;
      textBoxStatus.Clear();
      if (comboBox3_2.Text == "选择控制块")
      {
        textBoxStatus.Text = "请选择控制块";
        return;
      }

      if (comboBox3_1.Text == "KeyA")
        Reader.Cmd = Cmd.M1_KeyA_EncryptedSector;
      else if (comboBox3_1.Text == "KeyB")
        Reader.Cmd = Cmd.M1_KeyB_EncryptedSector;
      else
        return;
      Reader.Addr = Convert.ToByte(textBox1.Text, 16);
      Reader.M1Block = Convert.ToByte(comboBox3_2.Text, 10);
      Reader.Beep = Beep.On;

      status = Reader.M1_Operation();
      if (status == 0)
      {
        textBoxStatus.Text = "扇区加密成功";
      }
      else
      {
        textBoxStatus.Text = "错误码：" + status.ToString();
      }
    }
    private void button7_Click(object sender, EventArgs e)
    {
      int status;
      textBoxStatus.Clear();
      if (textBox4_3.Text == "")
      {
        textBoxStatus.Text = "数值不能为空";
        return;
      }
      if (comboBox4_2.Text == "选择数据块")
      {
        textBoxStatus.Text = "请选择数据块";
        return;
      }

      if (comboBox4_1.Text == "KeyA")
        Reader.Cmd = Cmd.M1_KeyA_InitVal;
      else if (comboBox4_1.Text == "KeyB")
        Reader.Cmd = Cmd.M1_KeyB_InitVal;
      else
        return;
      Reader.Addr = Convert.ToByte(textBox1.Text, 16);
      Reader.M1Block = Convert.ToByte(comboBox4_2.Text, 10);
      Reader.Beep = Beep.On;

      Int32 initvalue = Convert.ToInt32(textBox4_3.Text);

      Reader.TxBuffer[0] = (byte)(initvalue & 0xFF);
      Reader.TxBuffer[1] = (byte)((initvalue & 0xFF00) >> 8);
      Reader.TxBuffer[2] = (byte)((initvalue & 0xFF0000) >> 16);
      Reader.TxBuffer[3] = (byte)((initvalue >> 24) & 0xFF);
      status = Reader.M1_Operation();
      if (status == 0)
      {
        byte[] value = new byte[4];

        for (int i = 0; i < 4; i++)
        {
          value[i] = Reader.RxBuffer[i];
        }
        Int32 pvalue = BitConverter.ToInt32(value, 0);
        textBox7_3.Text = pvalue.ToString();
        textBoxStatus.Text = "初始化钱包成功";
      }
      else
      {
        textBoxStatus.Text = "错误码：" + status.ToString();
      }
    }

    private void button8_Click(object sender, EventArgs e)
    {
      int status;
      textBoxStatus.Clear();
      if (textBox5_3.Text == "")
      {
        textBoxStatus.Text = "数值不能为空";
        return;
      }
      if (comboBox5_2.Text == "选择数据块")
      {
        textBoxStatus.Text = "请选择数据块";
        return;
      }
      if (comboBox5_1.Text == "KeyA")
        Reader.Cmd = Cmd.M1_KeyA_Decrement;
      else if (comboBox5_1.Text == "KeyB")
        Reader.Cmd = Cmd.M1_KeyB_Decrement;
      else
        return;
      Reader.Addr = Convert.ToByte(textBox1.Text, 16);
      Reader.M1Block = Convert.ToByte(comboBox5_2.Text, 10);
      Reader.Beep = Beep.On;

      Int32 decvalue = Convert.ToInt32(textBox5_3.Text);

      Reader.TxBuffer[0] = (byte)(decvalue & 0xFF);
      Reader.TxBuffer[1] = (byte)((decvalue & 0xFF00) >> 8);
      Reader.TxBuffer[2] = (byte)((decvalue & 0xFF0000) >> 16);
      Reader.TxBuffer[3] = (byte)((decvalue >> 24) & 0xFF);

      status = Reader.M1_Operation();
      if (status == 0)
      {
        byte[] value = new byte[4];

        for (int i = 0; i < 4; i++)
        {
          value[i] = Reader.RxBuffer[i];
        }
        Int32 pvalue = BitConverter.ToInt32(value, 0);
        textBox7_3.Text = pvalue.ToString();
        textBoxStatus.Text = "钱包减值成功";
      }
      else
      {
        textBoxStatus.Text = "错误码：" + status.ToString();
      }
    }

    private void button9_Click(object sender, EventArgs e)
    {
      int status;
      textBoxStatus.Clear();
      if (textBox6_3.Text == "")
      {
        textBoxStatus.Text = "数值不能为空";
        return;
      }
      if (comboBox6_2.Text == "选择数据块")
      {
        textBoxStatus.Text = "请选择数据块";
        return;
      }

      if (comboBox6_1.Text == "KeyA")
        Reader.Cmd = Cmd.M1_KeyA_Increment;
      else if (comboBox6_1.Text == "KeyB")
        Reader.Cmd = Cmd.M1_KeyB_Increment;
      else
        return;
      Reader.Addr = Convert.ToByte(textBox1.Text, 16);
      Reader.M1Block = Convert.ToByte(comboBox6_2.Text, 10);
      Reader.Beep = Beep.On;

      Int32 incvalue = Convert.ToInt32(textBox6_3.Text);
      Reader.TxBuffer[0] = (byte)(incvalue & 0xFF);
      Reader.TxBuffer[1] = (byte)((incvalue & 0xFF00) >> 8);
      Reader.TxBuffer[2] = (byte)((incvalue & 0xFF0000) >> 16);
      Reader.TxBuffer[3] = (byte)((incvalue >> 24) & 0xFF);

      status = Reader.M1_Operation();
      if (status == 0)
      {
        byte[] value = new byte[4];
        for (int i = 0; i < 4; i++)
        {
          value[i] = Reader.RxBuffer[i];
        }
        Int32 pvalue = BitConverter.ToInt32(value, 0);
        textBox7_3.Text = pvalue.ToString();
        textBoxStatus.Text = "钱包增值成功";
      }
      else
      {
        textBoxStatus.Text = "错误码：" + status.ToString();
      }
    }

    private void button10_Click(object sender, EventArgs e)
    {
      int status;
      textBoxStatus.Clear();
      textBox7_3.Clear();

      if (comboBox7_2.Text == "选择数据块")
      {
        textBoxStatus.Text = "请选择数据块";
        return;
      }

      if (comboBox7_1.Text == "KeyA")
        Reader.Cmd = Cmd.M1_KeyA_ReadVal;
      else if (comboBox7_1.Text == "KeyB")
        Reader.Cmd = Cmd.M1_KeyB_ReadVal;
      else
        return;
      Reader.Addr = Convert.ToByte(textBox1.Text, 16);
      Reader.M1Block = Convert.ToByte(comboBox7_2.Text, 10);
      Reader.Beep = Beep.On;

      status = Reader.M1_Operation();
      if (status == 0)
      {
        byte[] value = new byte[4];
        for (int i = 0; i < 4; i++)
        {
          value[i] = Reader.RxBuffer[i];
        }
        Int32 pvalue = BitConverter.ToInt32(value, 0);
        textBox7_3.Text = pvalue.ToString();
        textBoxStatus.Text = "钱包查询成功";
      }
      else
      {
        textBoxStatus.Text = "错误码：" + status.ToString();
      }
    }
  }
}
