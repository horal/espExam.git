﻿namespace WindowsFormsApplication1
{
  partial class Form1
  {
    /// <summary>
    /// 必需的设计器变量。
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// 清理所有正在使用的资源。
    /// </summary>
    /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows 窗体设计器生成的代码

    /// <summary>
    /// 设计器支持所需的方法 - 不要
    /// 使用代码编辑器修改此方法的内容。
    /// </summary>
    private void InitializeComponent()
    {
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.button2 = new System.Windows.Forms.Button();
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.button3 = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.button5 = new System.Windows.Forms.Button();
      this.textBox2_3 = new System.Windows.Forms.TextBox();
      this.comboBox2_2 = new System.Windows.Forms.ComboBox();
      this.comboBox2_1 = new System.Windows.Forms.ComboBox();
      this.richTextBox1 = new System.Windows.Forms.RichTextBox();
      this.SuspendLayout();
      // 
      // textBox1
      // 
      this.textBox1.Location = new System.Drawing.Point(344, 15);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(79, 21);
      this.textBox1.TabIndex = 1;
      this.textBox1.Text = "20";
      this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // button2
      // 
      this.button2.Location = new System.Drawing.Point(12, 13);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(75, 23);
      this.button2.TabIndex = 2;
      this.button2.Text = "打开端口";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new System.EventHandler(this.button2_Click);
      // 
      // comboBox1
      // 
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new System.Drawing.Point(107, 15);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size(82, 20);
      this.comboBox1.TabIndex = 3;
      // 
      // button3
      // 
      this.button3.Location = new System.Drawing.Point(195, 12);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(75, 23);
      this.button3.TabIndex = 4;
      this.button3.Text = "搜索端口";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new System.EventHandler(this.button3_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(286, 19);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(41, 12);
      this.label1.TabIndex = 5;
      this.label1.Text = "设备号";
      // 
      // button5
      // 
      this.button5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
      this.button5.Location = new System.Drawing.Point(19, 62);
      this.button5.Name = "button5";
      this.button5.Size = new System.Drawing.Size(100, 23);
      this.button5.TabIndex = 12;
      this.button5.Text = "写数据到指定块";
      this.button5.UseVisualStyleBackColor = true;
      this.button5.Click += new System.EventHandler(this.button5_Click);
      // 
      // textBox2_3
      // 
      this.textBox2_3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
      this.textBox2_3.Location = new System.Drawing.Point(283, 63);
      this.textBox2_3.MaxLength = 32;
      this.textBox2_3.Name = "textBox2_3";
      this.textBox2_3.Size = new System.Drawing.Size(262, 21);
      this.textBox2_3.TabIndex = 14;
      this.textBox2_3.Text = "112233445566778899AABBCCDDEEFF00";
      // 
      // comboBox2_2
      // 
      this.comboBox2_2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
      this.comboBox2_2.FormattingEnabled = true;
      this.comboBox2_2.Items.AddRange(new object[] {
            "选择数据块",
            "1",
            "2",
            "4",
            "5",
            "6",
            "8",
            "9",
            "10",
            "12",
            "13",
            "14",
            "16",
            "17",
            "18",
            "20",
            "21",
            "22",
            "24",
            "25",
            "26",
            "28",
            "29",
            "30",
            "32",
            "33",
            "34",
            "36",
            "37",
            "38",
            "40",
            "41",
            "42",
            "44",
            "45",
            "46",
            "48",
            "49",
            "50"});
      this.comboBox2_2.Location = new System.Drawing.Point(186, 63);
      this.comboBox2_2.Name = "comboBox2_2";
      this.comboBox2_2.Size = new System.Drawing.Size(92, 20);
      this.comboBox2_2.TabIndex = 13;
      this.comboBox2_2.Text = "选择数据块";
      // 
      // comboBox2_1
      // 
      this.comboBox2_1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
      this.comboBox2_1.FormattingEnabled = true;
      this.comboBox2_1.Items.AddRange(new object[] {
            "KeyA",
            "KeyB"});
      this.comboBox2_1.Location = new System.Drawing.Point(124, 63);
      this.comboBox2_1.Name = "comboBox2_1";
      this.comboBox2_1.Size = new System.Drawing.Size(54, 20);
      this.comboBox2_1.TabIndex = 19;
      this.comboBox2_1.Text = "KeyA";
      // 
      // richTextBox1
      // 
      this.richTextBox1.Location = new System.Drawing.Point(19, 88);
      this.richTextBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
      this.richTextBox1.Name = "richTextBox1";
      this.richTextBox1.Size = new System.Drawing.Size(526, 286);
      this.richTextBox1.TabIndex = 41;
      this.richTextBox1.Text = "";
      this.richTextBox1.DoubleClick += new System.EventHandler(this.richTextBox1_DoubleClick);
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(560, 393);
      this.Controls.Add(this.richTextBox1);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.button3);
      this.Controls.Add(this.comboBox1);
      this.Controls.Add(this.button2);
      this.Controls.Add(this.textBox1);
      this.Controls.Add(this.comboBox2_2);
      this.Controls.Add(this.textBox2_3);
      this.Controls.Add(this.button5);
      this.Controls.Add(this.comboBox2_1);
      this.Name = "Form1";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "HF读写器测试软件（DLL）V1.0.0";
      this.Load += new System.EventHandler(this.Form1_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.ComboBox comboBox1;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button button5;
    private System.Windows.Forms.TextBox textBox2_3;
    private System.Windows.Forms.ComboBox comboBox2_2;
    private System.Windows.Forms.ComboBox comboBox2_1;
    private System.Windows.Forms.RichTextBox richTextBox1;
  }
}

