﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using HFrfid;

namespace WindowsFormsApplication1
{
  public partial class Form1 : Form
  {

    public Form1()
    {
      InitializeComponent();

    }

    RfidReader Reader = new RfidReader();

    public void SearchPort()  //搜索端口
    {
      string[] ports = SerialPort.GetPortNames();
      foreach (string port in ports)
      {
        comboBox1.Items.Add(port);
      }

      if (ports.Length > 0)
      {
        comboBox1.Text = ports[0];
      }
      else
      {
        MessageBox.Show("没有发现可用端口");
      }
    }

    public string byteToHexStr(byte[] bytes, int len)  //数组转十六进制字符
    {
      string returnStr = "";
      if (bytes != null)
      {
        for (int i = 0; i < len; i++)
        {
          returnStr += bytes[i].ToString("X2");
        }
      }
      return returnStr;
    }

    private static byte[] strToToHexByte(string hexString) //字符串转16进制
    {
      //hexString = hexString.Replace(" ", " "); 
      if ((hexString.Length % 2) != 0)
        hexString = "0" + hexString;
      byte[] returnBytes = new byte[hexString.Length / 2];
      for (int i = 0; i < returnBytes.Length; i++)
        returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
      return returnBytes;
    }
    private void Form1_Load(object sender, EventArgs e)
    {
      comboBox1.Items.Clear();
      comboBox1.Text = null;
      SearchPort();
    }
    private void button3_Click(object sender, EventArgs e)
    {
      comboBox1.Items.Clear();
      comboBox1.Text = null;
      SearchPort();
      Reader.DisConnect();
      button2.Text = "打开端口";
    }

    private void button2_Click(object sender, EventArgs e)
    {
      try
      {
        if (button2.Text == "打开端口")
        {
          bool flg = Reader.Connect(comboBox1.Text, 9600);
          if (flg == true)
          {
            button2.Text = "关闭端口";
          }
          else
          {
            MessageBox.Show("端口无法打开");
          }
        }
        else
        {
          Reader.DisConnect();
          button2.Text = "打开端口";
        }
      }
      catch
      {
        richTextBox1.AppendText("端口被占用\r\n");
      }
    }
    private void button1_Click(object sender, EventArgs e)
    {
      int status;
      byte[] type = new byte[2];
      byte[] id = new byte[4];
      textBox5.Clear();
      textBox2.Clear();

      Reader.Cmd = Cmd.M1_ReadId;//读卡号命令
      Reader.Addr = Convert.ToByte(textBox1.Text, 16);//读写器地址,设备号
      Reader.Beep = Beep.On;

      status = Reader.M1_Operation();
      if (status == 0)//读卡成功
      {
        for (int i = 0; i < 2; i++)//获取2字节卡类型
        {
          type[i] = Reader.RxBuffer[i];
        }
        for (int i = 0; i < 04; i++)//获取4字节卡号
        {
          id[i] = Reader.RxBuffer[i + 2];
        }
        string ss = byteToHexStr(type, 2);
        textBox2.Text = ss;
        ss = byteToHexStr(id, 4);
        textBox5.Text = ss;
        richTextBox1.AppendText("读卡号成功\r\n");
      }
      else
      {
        richTextBox1.AppendText("错误码：" + status.ToString() + "\r\n");
      }
    }
    private void richTextBox1_DoubleClick(object sender, EventArgs e)
    {
      richTextBox1.Clear();
      richTextBox1.AppendText("******DLL版本：" + Cmd.SolfVerion + "鼠标左键双击清空信息******\r\n");
    }
  }
}
   
