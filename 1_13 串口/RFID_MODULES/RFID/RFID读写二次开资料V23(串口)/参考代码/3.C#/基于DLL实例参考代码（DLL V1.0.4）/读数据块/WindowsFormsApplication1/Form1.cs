﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using HFrfid;

namespace WindowsFormsApplication1
{
  public partial class Form1 : Form
  {

    public Form1()
    {
      InitializeComponent();
      
    }

    RfidReader Reader = new RfidReader();
   
    public void SearchPort()  //搜索端口
    {
      string[] ports = SerialPort.GetPortNames();
      foreach (string port in ports)
      {
        comboBox1.Items.Add(port);
      }

      if (ports.Length > 0)
      {
        comboBox1.Text = ports[0];
      }
      else
      {
        MessageBox.Show("没有发现可用端口");
      }
    }

    public string byteToHexStr(byte[] bytes, int len)  //数组转十六进制字符
    {
      string returnStr = "";
      if (bytes != null)
      {
        for (int i = 0; i < len; i++)
        {
          returnStr += bytes[i].ToString("X2");
        }
      }
      return returnStr;
    }

    private static byte[] strToToHexByte(string hexString) //字符串转16进制
    {
      //hexString = hexString.Replace(" ", " "); 
      if ((hexString.Length % 2) != 0)
        hexString = "0" + hexString;
      byte[] returnBytes = new byte[hexString.Length / 2];
      for (int i = 0; i < returnBytes.Length; i++)
        returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
      return returnBytes;
    }
    private void Form1_Load(object sender, EventArgs e)
    {
      comboBox1.Items.Clear();
      comboBox1.Text = null;
      SearchPort();
      richTextBox1.AppendText("******DLL版本："+Cmd.SolfVerion+"鼠标左键双击清空信息******\r\n");
    }
    private void button3_Click(object sender, EventArgs e)
    {
      comboBox1.Items.Clear();
      comboBox1.Text = null;
      SearchPort();
      Reader.DisConnect();
      button2.Text = "打开端口";
    }

    private void button2_Click(object sender, EventArgs e)
    {
      try
      {
        if (button2.Text == "打开端口")
        {
          bool flg = Reader.Connect(comboBox1.Text, 9600);
          if (flg == true)
          {
            button2.Text = "关闭端口";
          }
          else
          {
            MessageBox.Show("端口无法打开");
          }
        }
        else
        {
          Reader.DisConnect();
          button2.Text = "打开端口";
        }
      }
      catch
      {
        richTextBox1.AppendText("端口被占用\r\n");
      }
    }

    private void button4_Click(object sender, EventArgs e)
    {
      int status;
      byte[] blockdata = new byte[16];
      textBox1_3.Clear();

      if (comboBox1_2.Text == "选择数据块")
      {
        richTextBox1.AppendText("操作错误，请选择数据块\r\n");
        return;
      }

      if (comboBox1_1.Text == "KeyA")
        Reader.Cmd = Cmd.M1_KeyA_ReadBlock; //读卡数据块命令，验证KeyA
      else if (comboBox1_1.Text == "KeyB")
        Reader.Cmd = Cmd.M1_KeyB_ReadBlock; //读卡数据块命令，验证KeyB
      else
        return;

      Reader.Addr = Convert.ToByte(textBox1.Text, 16); //读写器地址
      Reader.M1Block = Convert.ToByte(comboBox1_2.Text, 10);//数据块块号
      Reader.Beep = Beep.On;//读数据块成功蜂鸣器提示

      status = Reader.M1_Operation();//读操作
      if (status == 0)//读成功
      {
        for (int i = 0; i < 16; i++)
        {
          blockdata[i] = Reader.RxBuffer[i];//获取读到的数据，将读到的数据拷贝到blockdata数组
        }
        string ss = byteToHexStr(blockdata, 16); //将读取的数据转换成字符串
        textBox1_3.Text = ss;
        richTextBox1.AppendText("读指定块数据成功\r\n");
      }
      else
      {
        richTextBox1.AppendText("错误码：" + status.ToString() + "\r\n");
      }
    }
    private void richTextBox1_DoubleClick(object sender, EventArgs e)
    {
      richTextBox1.Clear();
      richTextBox1.AppendText("******DLL版本：" + Cmd.SolfVerion + "鼠标左键双击清空信息******\r\n");
    }
  }
}
