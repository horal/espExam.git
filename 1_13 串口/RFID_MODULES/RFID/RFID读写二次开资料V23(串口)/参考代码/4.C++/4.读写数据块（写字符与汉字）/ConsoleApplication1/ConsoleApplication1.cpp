// ConsoleApplication1.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"  
#include "SerialPort.h"
#include <process.h>  
#include <iostream>

using namespace std;

UCHAR  Cmd[23] = { 0x01, 0x17, 0xA4, 0x20, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

void CheckSumOut(UCHAR *buf, UCHAR len)
{
	UCHAR i;
	UCHAR checksum;
	checksum = 0;
	for (i = 0; i < (len - 1); i++)
	{
		checksum ^= buf[i];
	}
	buf[len - 1] = (UCHAR)~checksum;
}

bool CheckSumIn(UCHAR *buf, UCHAR len)
{
	UCHAR i;
	UCHAR checksum;
	checksum = 0;
	for (i = 0; i < (len - 1); i++)
	{
		checksum ^= buf[i];
	}
	if (buf[len - 1] == (UCHAR)~checksum)
	{
		return true;
	}
	return false;
}
int main(int argc, _TCHAR* argv[])
{
	CHAR status;
	UCHAR inbyte;
	UCHAR indata[100];
	INT block;
	UCHAR revdata[100];
	UINT len = 0;
	UINT readbytes;
	UINT i;

	SerialPort mySerialPort;

	if (!mySerialPort.InitPort(3)) //初始化COM3，并打开COM3
	{
		cout << "初始化COM3失败，请检查读写器端口是否为COM3，或者是否被其它软件打开占用！" << endl;
		cout << "按任意键后，回车退出程序！" << endl;
		cin >> inbyte;
	}
	else
	{
		cout << "初始化COM3成功!" << endl;
		cout << "输入 “1” 按回车键写数据块" << endl;
		cout << "输入 “2” 按回车键读数据块" << endl;
		while (true)
		{
			cin >> inbyte;
			block = -1;
			status = -1;
			switch (inbyte)
			{
				case '1':
					cout << "请将IC卡放读写器感应区内，输入要写入数据的块号（比如：1，2，4，5，6，8等）按回车键" << endl;
					cin >> block;
					if (block > 0)
					{
						cout << "输入16个要写入的字符或8个汉字按回车键（IC卡一个数据块为16字节，一个字符占用一个字节，一个汉字占用2个字节）" << endl;
						for (i = 0; i < 16; i++)
						{
							indata[i] = 0;
						}
						cin >> indata;
						Cmd[1] = 0x17;
						Cmd[2] = 0xA4;
						Cmd[4] = (UCHAR)block;
						for (i = 0; i < 16; i++) //拷贝要写入的数据到数组中
						{
							Cmd[6 + i] = indata[i];
						}
					}
					break;
				case '2':
					cout << "请将IC卡放读写器感应区内，输入要读的块号（比如：1，2，4，5，6，8等）按回车开始读卡……" << endl;
					cin >> block;
					if (block > 0)
					{
						Cmd[1] = 0x08;
						Cmd[2] = 0xA3;
						Cmd[4] = (UCHAR)block;
					}
					break;
				default:
					cout << "******输入错误！输入错误！输入错误！******" << endl << endl;
					cout << "输入 “1” 按回车键写数据块" << endl;
					cout << "输入 “2” 按回车键读数据块" << endl;
			}
			if (block > 0)
			{
				CheckSumOut(Cmd, Cmd[1]);
				mySerialPort.WriteData(Cmd, Cmd[1]);  //通过串口发送读数据块指令给读写器
				Sleep(200); // 延时200毫秒等待读写器返回数据，延时太小可能无法接收完整的数据包
				len = mySerialPort.GetBytesInCOM(); //获取串口缓冲区中字节数
				if (len >= 8) 
				{
					readbytes = 0;
					do // 获取串口缓冲区数据
					{
						inbyte = 0;
						if (mySerialPort.ReadChar(inbyte) == true)
						{
							revdata[readbytes] = inbyte;
							readbytes++;
						}
					} while (--len);
					if ((revdata[0] = 0x01) && (revdata[1] == 8) && (readbytes == revdata[1]) && (revdata[2] == 0x0A4) && (revdata[3] = 0x20)) //判断是否为写数据返回的数据包
					{
						bool status = CheckSumIn(revdata, revdata[1]);  //计算校验和
						if (status)
						{
							if (revdata[4] == 0x00)
							{
								cout << "写数据到数据块" << block << "成功！" << endl << endl;
							}
							else
							{
								cout << "读,写数据块失败,失败原因如下：" << endl;
								cout << "1. 检查IC卡是否放置在读写器的感应区内." << endl;
								cout << "2. IC卡对应扇区密码与读写器读写密码不一致." << endl;
								cout << "3. 输入的数据块值超过IC卡的最大数据块数值，比如S50卡有63个数据块."  << endl;
								cout << "4. 密码控制块不可以读或写." << endl;
							}
						}
					}
					if (((revdata[0] = 0x01) && ((revdata[1] == 8) || (revdata[1] == 22)) && (readbytes == revdata[1]) && (revdata[2] == 0x0A3) && (revdata[3] = 0x20)))//判断是否为读数据块返回的数据包
					{
						bool status = CheckSumIn(revdata, revdata[1]);
						if (status)
						{
							if (revdata[4] == 0x00)
							{
								UCHAR blockdata[17];
								for (i = 0; i<16; i ++)
								{
									blockdata[i] = revdata[5 + i];
								}
								//blockdata[16] = '\0';
								cout << "读数据块成功，数据块" << block << "数据为：" << blockdata << endl << endl;
							}
							else
							{
								cout << "读,写数据块失败,失败原因如下：" << endl;
								cout << "1. 检查IC卡是否放置在读写器的感应区内." << endl;
								cout << "2. IC卡对应扇区密码与读写器读写密码不一致." << endl;
								cout << "3. 输入的数据块值超过IC卡的最大数据块数值，比如S50卡有63个数据块." << endl;
								cout << "4. 密码控制块不可以读或写." << endl;
							}
						}
					}
				}
				else
				{
					cout << "读写器超时……，请检查读卡器的连接是否正常！" << endl;
					while (len > 0) //如果缓冲区中有数据，将缓冲区中数据清空
					{
						mySerialPort.ReadChar(inbyte);
					}
				}
			}
			else
			{
				if (cin.fail())
				{
					cin.clear();
					cin.sync();
					cout << "******输入错误，请输入数字******" << endl << endl;
					cout << "输入 “1” 按回车键写数据块" << endl;
					cout << "输入 “2” 按回车键读数据块" << endl;
				}
			}
		}
	}
}

