// ConsoleApplication1.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"  
#include "SerialPort.h"
#include <process.h>  
#include <iostream>

using namespace std;

void CheckSumOut(UCHAR *buf, UCHAR len)
{
	UCHAR i;
	UCHAR checksum;
	checksum = 0;
	for (i = 0; i < (len - 1); i++)
	{
		checksum ^= buf[i];
	}
	buf[len - 1] = (UCHAR)~checksum;
}

bool CheckSumIn(UCHAR *buf, UCHAR len)
{
	UCHAR i;
	UCHAR checksum;
	checksum = 0;
	for (i = 0; i < (len - 1); i++)
	{
		checksum ^= buf[i];
	}
	if (buf[len - 1] == (UCHAR)~checksum)
	{
		return true;
	}
	return false;
}
int main(int argc, _TCHAR* argv[])
{
	UCHAR inbyte;
	UINT indata;
	INT block;
	UCHAR revdata[100];
	UINT len = 0;
	UINT readbytes;
	UINT i;
	UCHAR  Cmd[11] ;
	SerialPort mySerialPort;

	if (!mySerialPort.InitPort(3)) //初始化COM3，并打开COM3
	{
		cout << "初始化COM3失败，请检查读写器端口是否为COM3，或者是否被其它软件打开占用！" << endl;
		cout << "按任意键后，回车退出程序！" << endl;
		cin >> inbyte;
	}
	else
	{
		cout << "初始化COM3成功!" << endl << endl;
		cout << "输入“1” 按回车键给钱包加值" << endl;
		cout << "输入“2” 按回车键查询钱包" << endl;
		while (true)
		{
			cin >> inbyte;
			block = -1;
			switch (inbyte)
			{
				case '1':
					cout << "请将IC卡放读写器感应区内，输入要加值的数据块块号（比如：1，2，4，5，6，8等）按回车键" << endl;
					cin >> block;
					if (block > 0)
					{
						cout << "输入要加值的数值按回车键加值" << endl;
						cin >> indata;
						Cmd[0] = 0x01; //命令格式请核对协议手册充值章节
						Cmd[1] = 0x0B;
						Cmd[2] = 0xA8; //充值指令 
						Cmd[3] = 0x20;
						Cmd[4] = (UCHAR)block;
						Cmd[5] = 0x01;
						Cmd[6] = (UCHAR)indata;
						Cmd[7] = (UCHAR)(indata>>8);
						Cmd[8] = (UCHAR)(indata >> 16);
						Cmd[9] = (UCHAR)(indata >> 24);
					}
					break;
				case '2':
					cout << "请将IC卡放读写器感应区内，输入要查询的数据块号（比如：1，2，4，5，6，8等）按回车键查询" << endl;
					cin >> block;
					if (block > 0)
					{
						Cmd[0] = 0x01;
						Cmd[1] = 0x08;
						Cmd[2] = 0xA9;
						Cmd[3] = 0x20;
						Cmd[4] = (UCHAR)block;
						Cmd[5] = 0x01;
						Cmd[6] = 0x00;
					}
					break;
				default:
					cout << "******输入错误！输入错误！输入错误！******" << endl << endl;
					cout << "输入“1” 按回车键给钱包加值" << endl;
					cout << "输入“2” 按回车键查询钱包" << endl;
			}
			if (block > 0)
			{
				CheckSumOut(Cmd, Cmd[1]);
				mySerialPort.WriteData(Cmd, Cmd[1]);  //通过串口发送读数据块指令给读写器
				Sleep(200); // 延时200毫秒等待读写器返回数据，延时太小可能无法接收完整的数据包
				len = mySerialPort.GetBytesInCOM(); //获取串口缓冲区中字节数
				if (len >= 8) 
				{
					readbytes = 0;
					do // 获取串口缓冲区数据
					{
						inbyte = 0;
						if (mySerialPort.ReadChar(inbyte) == true)
						{
							revdata[readbytes] = inbyte;
							readbytes++;
						}
					} while (--len);
					if ((revdata[0] = 0x01) &&( (revdata[1] == 8) || (revdata[1] == 10) )&& (revdata[1] == readbytes) && (revdata[2] == 0xA8) && (revdata[3] = 0x20)) //判断返回的数据包
					{
						bool status = CheckSumIn(revdata, revdata[1]);  //计算校验和
						if (status)
						{
							if (revdata[4] == 0x00) //成功
							{
								INT blockdata;
								blockdata = revdata[5];
								blockdata += (INT)revdata[6] << 8;
								blockdata += (INT)revdata[7] << 16;
								blockdata += (INT)revdata[8] << 24;
								cout << "数据块" << block << "加值成功！" << "余额为：" << blockdata << endl << endl;
							}
							else  //失败
							{
								cout << "钱包操作失败,失败原因如下：" << endl;
								cout << "1. 检查IC卡是否放置在读写器的感应区内." << endl;
								cout << "2. IC卡对应扇区密码与读写器读写密码不一致." << endl;
								cout << "3. 输入的数据块值超过IC卡的最大数据块数值，比如S50卡有63个数据块." << endl;
								cout << "4. 密码控制块不可以读写，也不可以作为钱包使用." << endl;
								cout << "5. 块0只读，不能写，也不能作为钱包使用." << endl;
								cout << "6. 操作的数据块是否为钱包格式，如果不是请先初始化为钱包格式." << endl;
							}
						}
					}
					if ((revdata[0] = 0x01) && ((revdata[1] == 8) || (revdata[1] == 10)) && (revdata[1] == readbytes) && (revdata[2] == 0xA9) && (revdata[3] = 0x20)) //判断返回的数据包
					{
						bool status = CheckSumIn(revdata, revdata[1]); //计算校验和
						if (status)
						{
							if (revdata[4] == 0x00) //成功
							{
								INT blockdata; 

								blockdata = revdata[5];
								blockdata += (INT)revdata[6] << 8;
								blockdata += (INT)revdata[7] << 16;
								blockdata += (INT)revdata[8] << 24;

								cout << "查询数据块" << block << "余额成功," << "余额为：" << blockdata << endl << endl;
							}
							else //失败
							{
								cout << "钱包操作失败,失败原因如下：" << endl;
								cout << "1. 检查IC卡是否放置在读写器的感应区内." << endl;
								cout << "2. IC卡对应扇区密码与读写器读写密码不一致." << endl;
								cout << "3. 输入的数据块值超过IC卡的最大数据块数值，比如S50卡有63个数据块." << endl;
								cout << "4. 密码控制块不可以读写，也不可以作为钱包使用." << endl;
								cout << "5. 块0只读，不能写，也不能作为钱包使用." << endl;
								cout << "6. 操作的数据块是否为钱包格式，如果不是请先初始化为钱包格式." << endl;
							}
						}
					}
				}
				else
				{
					while (len > 0) //如果缓冲区中有数据，将缓冲区中数据清空
					{
						mySerialPort.ReadChar(inbyte);
					}
					cout << "读写器返回数据超时……，请检查读卡器的连接是否正常！" << endl;
				}
			}
			else
			{
				if (cin.fail())
				{
					cin.clear();
					cin.sync();
					cout << "******输入错误，请输入数字******" << endl << endl;
					cout << "输入“1” 按回车键给钱包加值" << endl;
					cout << "输入“2” 按回车键查询钱包" << endl;
				}
			}
		}
	}
}

