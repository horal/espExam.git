// ConsoleApplication1.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"  
#include "SerialPort.h"
#include <process.h>  
#include <iostream>

using namespace std;

void CheckSumOut(UCHAR *buf, UCHAR len)
{
	UCHAR i;
	UCHAR checksum;
	checksum = 0;
	for (i = 0; i < (len - 1); i++)
	{
		checksum ^= buf[i];
	}
	buf[len - 1] = (UCHAR)~checksum;
}

bool CheckSumIn(UCHAR *buf, UCHAR len)
{
	UCHAR i;
	UCHAR checksum;
	checksum = 0;
	for (i = 0; i < (len - 1); i++)
	{
		checksum ^= buf[i];
	}
	if (buf[len - 1] == (UCHAR)~checksum)
	{
		return true;
	}
	return false;
}
int main(int argc, _TCHAR* argv[])
{
	UCHAR inbyte;
	INT block;
	UCHAR revdata[32];
	UINT len = 0;
	UINT readbytes;
	UINT i;
	UCHAR cmd[8];

	SerialPort mySerialPort;

	if (!mySerialPort.InitPort(3)) //初始化COM3，并打开COM3
	{
		cout << "初始化COM3失败，请检查读写器端口是否为COM3，或者是否被其它软件打开占用！" << endl;
		cout << "按任意键后，回车退出程序！" << endl;
		cin >> block;
	}
	else
	{
		cout << "初始化COM3成功!" << endl;
		cout << "请将IC卡放读写器感应区内，输入要加密的扇区密码控制块编号（比如：3，7，11，19，43，51等）按回车键" << endl;
		while (true)
		{
			block = -1;
			cin >> block;
			if (block >= 0)
			{
				cmd[0] = 0x01;
				cmd[1] = 0x08;
				cmd[2] = 0xA5;
				cmd[3] = 0x20;
				cmd[4] = (UCHAR)block;
				cmd[5] = 0x01;
				cmd[6] = 0x00;
				CheckSumOut(cmd, cmd[1]);
				mySerialPort.WriteData(cmd, cmd[1]);  //通过串口发送读数据块指令给读写器
				Sleep(200); // 延时200毫秒等待读写器返回数据，延时太小可能无法接收完整的数据包
				len = mySerialPort.GetBytesInCOM(); //获取串口缓冲区中字节数
				if (len >= 8) 
				{
					readbytes = 0;
					do 
					{
						inbyte = 0;
						if (mySerialPort.ReadChar(inbyte) == true)
						{
							revdata[readbytes] = inbyte;
							readbytes++;
						}
					} while (--len); // 获取串口缓冲区数据
					if ((revdata[0] = 0x01) && (revdata[1] == 8) && (readbytes == revdata[1]) && (revdata[2] == 0xA5) && (revdata[3] = 0x20)) //对数据进行判断
					{
						bool status = CheckSumIn(revdata, revdata[1]);
						if (status)
						{
							if (revdata[4] == 0x00) //读数据块成功
							{
								cout << "恭喜，扇区加密成功！"<< endl;
							}
							else //读数据块失败
							{
								cout << "扇区加密失败,失败原因如下：" << endl;
								cout << "1. 检查IC卡是否放置在读写器的感应区内." << endl;
								cout << "2. IC卡当前对应扇区密码与读写器旧密码不一致." << endl;
								cout << "3. 输入的密码控制块值错误，每个扇区的最高数据块为密码控制控制，比如：第0扇区的密码控制块为3，第1扇区的密码控制块为7，以此类推." << endl;
							}
						}
					}
				}
				else
				{
					cout << "读写器超时……，请检查读卡器的连接是否正常！" << endl;
					while (len > 0)
					{
						mySerialPort.ReadChar(inbyte);
					}
				}
			}
			else
			{
				if (cin.fail())
				{
					cin.clear();
					cin.sync();
					cout << "请输入数字." << endl;
				}
			}
		}
	}
}

