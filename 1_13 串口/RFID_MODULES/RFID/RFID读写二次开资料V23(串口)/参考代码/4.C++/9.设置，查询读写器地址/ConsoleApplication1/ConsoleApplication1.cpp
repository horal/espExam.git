// ConsoleApplication1.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"  
#include "SerialPort.h"
#include <process.h>  
#include <iostream>

using namespace std;

void CheckSumOut(UCHAR *buf, UCHAR len)
{
	UCHAR i;
	UCHAR checksum;
	checksum = 0;
	for (i = 0; i < (len - 1); i++)
	{
		checksum ^= buf[i];
	}
	buf[len - 1] = (UCHAR)~checksum;
}

bool CheckSumIn(UCHAR *buf, UCHAR len)
{
	UCHAR i;
	UCHAR checksum;
	checksum = 0;
	for (i = 0; i < (len - 1); i++)
	{
		checksum ^= buf[i];
	}
	if (buf[len - 1] == (UCHAR)~checksum)
	{
		return true;
	}
	return false;
}
int main(int argc, _TCHAR* argv[])
{
	CHAR status;
	UCHAR inbyte;
	INT addr;
	UCHAR revdata[100];
	UINT len = 0;
	UINT readbytes;
	UINT i;
	UCHAR  Cmd[8] ;
	SerialPort mySerialPort;
	
	if (!mySerialPort.InitPort(3)) //初始化COM3，并打开COM3
	{
		cout << "初始化COM3失败，请检查读写器端口是否为COM3，或者是否被其它软件打开占用！" << endl;
		cout << "按任意键后，回车退出程序！" << endl;
		cin >> inbyte;
	}
	else
	{
		cout << "初始化COM3成功!" << endl << endl;
		cout << "输入“1” 按回车键设置读写器地址" << endl;
		cout << "输入“2” 按回车键查询读写器地址" << endl;
		while (true)
		{
			status = -1;
			addr = -1;
			cin >> inbyte;
			switch (inbyte)
			{
				case '1':
					cout << "请输入地址数值（范围：1-255）按回车键" << endl;
					cin >> addr;
					if (addr > 0)
					{
						Cmd[0] = 0x03; //命令格式请核对协议手册充值章节
						Cmd[1] = 0x08;
						Cmd[2] = 0xC0; //查询地址
						Cmd[3] = 0x00;
						Cmd[4] = (UCHAR)addr;
						Cmd[5] = 0x00;
						Cmd[6] = 0x00;
						status = 1;
					}
					else if (cin.fail())
					{
						cin.clear();
						cin.sync();
						cout << "******输入错误，请输入数字******" << endl << endl;
						cout << "输入“1” 按回车键设置读写器地址" << endl;
						cout << "输入“2” 按回车键查询读写器地址" << endl;
					}
					break;
				case '2':
					Cmd[0] = 0x02;
					Cmd[1] = 0x08;
					Cmd[2] = 0xB0;
					Cmd[3] = 0x00;
					Cmd[4] = 0x00;
					Cmd[5] = 0x00;
					Cmd[6] = 0x00;
					status = 1;
					break;
				default:
					cout << "******输入错误！输入错误！输入错误！******" << endl << endl;
					cout << "输入“1” 按回车键设置读写器地址" << endl;
					cout << "输入“2” 按回车键查询读写器地址" << endl;
					status = -1;
			}
			if (status == 1)
			{
				CheckSumOut(Cmd, Cmd[1]);
				mySerialPort.WriteData(Cmd, Cmd[1]);  //通过串口发送读数据块指令给读写器
				Sleep(200); // 延时200毫秒等待读写器返回数据，延时太小可能无法接收完整的数据包
				len = mySerialPort.GetBytesInCOM(); //获取串口缓冲区中字节数
				if (len >= 8) 
				{
					readbytes = 0;
					do // 获取串口缓冲区数据
					{
						inbyte = 0;
						if (mySerialPort.ReadChar(inbyte) == true)
						{
							revdata[readbytes] = inbyte;
							readbytes++;
						}
					} while (--len);
					if ((revdata[0] == 0x03) &&( revdata[1] == 8 )&& (revdata[1] == readbytes) && (revdata[2] == 0xC0) && (revdata[3] == 0x00)) //判断是否为设置地址返回的数据包
					{
						bool status = CheckSumIn(revdata, revdata[1]);  //计算校验和
						if (status)
						{
							if (revdata[4] == 0x00) //成功
							{
								cout <<  "设置读写器地址成功!" << endl << endl;
							}
						}
					}
					else if ((revdata[0] ==0x02) && (revdata[1] == 8)&& (revdata[1] == readbytes) && (revdata[2] == 0xB0) && (revdata[3] == 0x00)) //判断是否为查询地址返回的数据包
					{
						bool status = CheckSumIn(revdata, revdata[1]); //计算校验和
						if (status)
						{
							if (revdata[4] == 0x00) //成功
							{
								cout << "查询读写器地址为：" << (UINT)revdata[5] << endl << endl;
							}
						}
					}
				}
				else
				{
					while (len > 0) //如果缓冲区中有数据，将缓冲区中数据清空
					{
						mySerialPort.ReadChar(inbyte);
					}
					cout << "读写器返回数据超时……，请检查读卡器的连接是否正常！" << endl;
				}
			}
		}
	}
}

