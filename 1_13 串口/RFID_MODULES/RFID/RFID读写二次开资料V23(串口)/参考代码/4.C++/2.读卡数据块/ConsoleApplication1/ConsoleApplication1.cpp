// ConsoleApplication1.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"  
#include "SerialPort.h"
#include <process.h>  
#include <iostream>

using namespace std;

UCHAR  CmdReadBlock[8] = { 0x01, 0x08, 0xA3, 0x20, 0x00, 0x01, 0x00, 0x00 }; //01 08 A3 20 01 01 00 75 

void CheckSumOut(UCHAR *buf, UCHAR len)
{
	UCHAR i;
	UCHAR checksum;
	checksum = 0;
	for (i = 0; i < (len - 1); i++)
	{
		checksum ^= buf[i];
	}
	buf[len - 1] = (UCHAR)~checksum;
}

bool CheckSumIn(UCHAR *buf, UCHAR len)
{
	UCHAR i;
	UCHAR checksum;
	checksum = 0;
	for (i = 0; i < (len - 1); i++)
	{
		checksum ^= buf[i];
	}
	if (buf[len - 1] == (UCHAR)~checksum)
	{
		return true;
	}
	return false;
}
//字节流转换为十六进制字符串的另一种实现方式  
void Hex2Str(const UCHAR *sSrc, UCHAR *sDest, int nSrcLen)
{
	int  i;
	char szTmp[3];

	for (i = 0; i < nSrcLen; i++)
	{
		sprintf_s(szTmp, "%02X", (unsigned char)sSrc[i]);
		memcpy(&sDest[i * 2], szTmp, 2);
	}
	sDest[nSrcLen * 2 ] = '\0';
	return;
}
int main(int argc, _TCHAR* argv[])
{
	UCHAR inbyte;
	INT block;
	UCHAR revdata[32];
	UINT len = 0;
	UINT readbytes;
	UINT i;

	SerialPort mySerialPort;

	if (!mySerialPort.InitPort(3)) //初始化COM3，并打开COM3
	{
		cout << "初始化COM3失败，请检查读写器端口是否为COM3，或者是否被其它软件打开占用！" << endl;
		cout << "按任意键后，回车退出程序！" << endl;
		cin >> block;
	}
	else
	{
		cout << "初始化COM3成功!" << endl;
		cout << "请将IC卡放读写器感应区内，输入要读取的数据块块号（比如：1，2，4，5，6，8等）按回车开始读卡……\n" << endl;
		while (true)
		{
			block = -1;
			cin >> block;
			if (block >= 0)
			{
				cout << "开始读数据块" << block << "数据…………" << endl;
				CmdReadBlock[4] = (UCHAR)block;
				CheckSumOut(CmdReadBlock, CmdReadBlock[1]);
				mySerialPort.WriteData(CmdReadBlock, CmdReadBlock[1]);  //通过串口发送读数据块指令给读写器
				Sleep(200); // 延时200毫秒等待读写器返回数据，延时太小可能无法接收完整的数据包
				len = mySerialPort.GetBytesInCOM(); //获取串口缓冲区中字节数
				if (len >= 8) 
				{
					readbytes = 0;
					do 
					{
						inbyte = 0;
						if (mySerialPort.ReadChar(inbyte) == true)
						{
							revdata[readbytes] = inbyte;
							readbytes++;
						}
					} while (--len); // 获取串口缓冲区数据
					if ((revdata[0] = 0x01) && ((revdata[1] == 8) || (revdata[1] == 22)) && (readbytes == revdata[1]) && (revdata[2] == 0xA3) && (revdata[3] = 0x20)) //对数据进行判断
					{
						bool status = CheckSumIn(revdata, revdata[1]);
						if (status)
						{
							if (revdata[4] == 0x00) //读数据块成功
							{
								UCHAR blockdata[16];
								UCHAR temp[33];
								for (i = 0; i < 16; i++)
								{
									blockdata[i] = revdata[5 + i]; //复制读到的数据块数据到数组blockdata中
								}
								Hex2Str(&blockdata[0], &temp[0], 16); // 数据块数据转换为字符
								cout << "读数据块成功，数据块" << block << "数据为：" << temp << endl;
							}
							else //读数据块失败
							{
								cout << "读数据块失败,失败原因如下：" << endl;
								cout << "1. 检查IC卡是否放置在读写器的感应区内." << endl;
								cout << "2. IC卡对应扇区密码与读写器读写密码不一致." << endl;
								cout << "3. 输入的数据块值超过IC卡的最大数据块数值，比如S50卡有63个数据块." << endl;
								cout << "4. 密码控制块不可以读." << endl;
							}
						}
					}
				}
				else
				{
					cout << "读写器超时……，请检查读卡器的连接是否正常！" << endl;
					while (len > 0)
					{
						mySerialPort.ReadChar(inbyte);
					}
				}
			}
			else
			{
				if (cin.fail())
				{
					cin.clear();
					cin.sync();
					cout << "请输入数字." << endl;
				}
			}
		}
	}
}

