/*********************************************************************
**  MCU:STC11F32XE LQFP-44							 
**  晶振：18.432M
**  编写:大叶先生
**  版权：深圳市昱闵科技有限公司		 	 专业RFID研发制造销售
*********************************************************************/
#include "include.h"

unsigned char const Cmd_AutoReadId[8] = {0x03, 0x08, 0xC1, 0x20, 0x02, 0x00, 0x00, 0x17 }; //主动读卡号模式命令
unsigned char const Cmd_AutoReadBlock[8] = {0x03, 0x08, 0xC1, 0x20, 0x03, 0x0A, 0x00, 0x1C }; //主动读第10块数据模式命令
unsigned char const Cmd_AutoReadIdAndBlock[8] = {0x03, 0x08, 0xC1, 0x20, 0x04, 0x0A, 0x00, 0x1B }; //主动读卡号+第10块数据模式命令

unsigned char data RxDataFlag = 0x00; 
unsigned char UartRxDataFlag = 0x00; 

unsigned char buf[MAX_PKT_LEN];

unsigned char BeepCount;

unsigned char RxDataCount = 0x0;
unsigned char xdata UartRxBuf[UART_RX_BUF_LEN];	
unsigned char CardData[20];		 

unsigned char BPass;

/*********************延时函数***********************/
void DelayMs(uint data tms)
{

	while(tms--)
	{
		unsigned char i, j;
//		_nop_();
//		_nop_();
		i = 18;
		j = 234;
		do
		{
			while (--j);
		} while (--i);
	}
}
/***********************蜂鸣器与LED提示**************************/
void Pass(void)
{
  SET_BEEP;
  Led1_On();
	Led2_On();
	Led3_On();
  DelayMs(200);
  CLR_BEEP;
  Led1_Off();
	Led2_Off();
	Led3_Off();
	DelayMs(200);
}

/**********************校验和计算**************************/
unsigned char CheckSum(unsigned char *ptr,unsigned char len)
{
	unsigned char i;
	unsigned char checksum;
	checksum = 0;
	for(i=0;i<(len-1);i++)
	{
		   checksum ^= ptr[i];
	}
	if(	ptr[len-1] == (~checksum))
		return 	0x01;
	else 
		return 	0x00;
}
/***********************初始化IO**************************/
void InitPort(void)
{  
  P1M1=0x00;
  P1M0=0x02;
  P11 = 0;
  P12 = 1;
  P13 = 1;
  P14 = 1;

  P3M1=0x00;
  P3M0=0x00;
  
  P4M1=0x00;
  P4M0=0x20;
  P4SW = P45EN;
  P45=0;
}
/*******************初始化定时器**************************/
//void InitTimer1(void)
//{
//
//	TMOD &= 0x0f;		
//	TMOD |= 0x10;		//设置定时器模式,选择定时器1，16位定时器
//	AUXR &= 0xbf;   //12分频
//
//	TL1 = 0x00;
//	TH1 = 0xc4;		 //10MS
//	TF1 = 0;			 // 清中断标志位
// 	ET1 = 1;			//开定时器1中断
//	TR1 = 1;			//启动定时器1
//}
/********************定时器1中断服务**************************/
//void IrqTimer1(void) interrupt 3
//{
//	TR1=0;
//	TL1 = 0x00;
//	TH1 = 0xc4;	
//	Timer1CleanDataCount++;
//	//Beep();
//	TR1=1;		
//}

/********************串口发送1字节数据**************************/
void SendChar(uchar data sentData)
{
  ES=0;
  TI=0;
  SBUF=sentData;
  while(TI==0);
  TI=0;
  ES=1;
}
/**********************串口发送数据************************/
void SendData(unsigned char *p,unsigned len)
{
	unsigned char i;
	Rs485_Tx_Mode();
	for(i=0;i<len;i++)
	{
		SendChar(p[i]);	
	}
	DelayMs(1);
	Rs485_Rx_Mode();
}
/***********************串口初始化***************************************/
void UartInit(void)
{
  SCON = 0x50;
  BRT=RELOAD_COUNT;
  //AUXR=0x11|(1<<2);	   //独立波特率发生器 1T 工作模式 ,115200
  AUXR=0x11;				//9600
  AUXR1=0x00;  //0x80:p1  0x00:p3
  ES=1;
}
///***********************串口发送字符串*******************************/
//void Send_String(unsigned char *string)
//{
//	while((*string) != '\0') 
//	{
//	 SendChar(*string++);
//	};
//}
/********************串口接收数据*************************************/
void uart_interrupt_receive(void) interrupt 4
{
  ES = 0;
  if(RI==1);
  {
    RI=0;
		if(UartRxDataFlag == 0x00) 
		{
			UartRxBuf[RxDataCount] = SBUF; 
			RxDataCount++; 
			if(RxDataCount >= UART_RX_BUF_LEN) 
			{
				RxDataCount = UART_RX_BUF_LEN -1;
			}
			if((UartRxBuf[1] == RxDataCount)&&(UartRxBuf[1] >= MIN_PKT_LEN)&&(UartRxBuf[1] <= MAX_PKT_LEN))	 //判断是否接收到一包数据
			{
				UartRxDataFlag = 0x01;
			  RxDataCount = 0x00;	
			}
			else if((UartRxBuf[1] < MIN_PKT_LEN )&&(UartRxBuf[1] > MAX_PKT_LEN))//无效包长度
			{
				RxDataCount = 0x00;	
			}
		}
  } 
  ES = 1;
}
/**********************系统初始化************************/
void InitAll(void)
{
  InitPort();
  DelayMs(1);
  Rs485_Rx_Mode();
  UartInit();
  DelayMs(1);
//  InitTimer1();
  EA=1; 
  BPass=0;

}
/**********************按键处理************************/
void Key(void)
{
	if(!KEY1)	//按键KEY1被按下的时候，设置读卡器的工作模式为主动读卡号模式
	{
		DelayMs(100);
		if(!KEY1)
		{
			SendData(Cmd_AutoReadId,8);
		}	
	}
	else if(!KEY2)//按键KEY2被按下的时候，设置读卡器的工作模式为主动读指定数据块模式
	{
		DelayMs(100);
		if(!KEY2)
		{
			SendData(Cmd_AutoReadBlock,8);
		}	
	}
	else if(!KEY3)//按键KEY3被按下的时候，设置读卡器的工作模式为主动读卡号+指定数据块模式
	{
		DelayMs(100);
		if(!KEY3)
		{
			SendData(Cmd_AutoReadIdAndBlock,8);	
		}
	}
}

/***********************串口接收数据处理**************************/
void ProcessUartRxData(void)
{
	unsigned char check;
	unsigned char pktlen;
	unsigned char i;
	if(UartRxDataFlag)//判断是否接收到一包数据
	{
		pktlen = UartRxBuf[1]; //获取包长度
		for(i=0;i<pktlen;i++)	 //拷贝数据包到数组缓冲区中
		{
 			buf[i] = UartRxBuf[i];	
		}
		UartRxDataFlag = 0x00; //允许串口继续接收数据
		check = CheckSum(buf,pktlen);	//对数组内数据进行校验
		if(check)
		{
			//判断解析主动读卡号的数据包
			if((buf[0] == 0x04)&&(buf[1] == 0x0C)&&(buf[2]== 0x02)&&(buf[3]==0x20)&&(buf[4]==0x00))
			{
				for(i=0;i<4;i++)		//获取数据包中的卡号
				{
 					CardData[i] = buf[(7+i)]; //卡号在数据包中位置是第7-10字节
				}
//					SendData(CardData,4);
				BPass = 0x01;//蜂鸣器提示成功
				BeepCount = 0x01;//蜂鸣器与LED提示1次
				return;
			}
			//判断解析主动读指定数据块的数据包
			else if((buf[0] == 0x04)&&(buf[1] == 0x16)&&(buf[2]== 0x03)&&(buf[3]==0x20)&&(buf[4]==0x00))
  		{
				for(i=0;i<16;i++)		//获取数据包中的块数据
				{
	 				CardData[i] = buf[(5+i)]; //块数据在数据包中位置是第5-20字节，一共16字节（1个数据块长度）	
				}
//					SendData(CardData,16);
				BPass = 0x01;//蜂鸣器提示成功
				BeepCount = 0x02;	//蜂鸣器与LED提示2次
				return;
			}
			//判断解析主动读卡号+指定数据块的数据包
			else if((buf[0] == 0x04)&&(buf[1] == 0x1C)&&(buf[2]== 0x04)&&(buf[3]==0x20)&&(buf[4]==0x00))
	  	{
				for(i=0;i<20;i++)		//获取数据包中的卡号与块数据
				{
	 				CardData[i] = buf[(7+i)]; //卡号与块数据在数据包中位置是第7-26字节，卡号4字节，块数据16字节，一共20字节
				}
//					SendData(CardData,20);
				BPass = 0x01;//蜂鸣器提示成功
				BeepCount = 0x03;//蜂鸣器与LED提示3次
				return;
			}
			//判断解析通过按键设置工作模式，设置成功读写器返回的数据包
			else if((buf[0]== 0x03)&&(buf[1] == 0x08)&&(buf[2]== 0xC1)&&(buf[3]==0x20)&&(buf[4]==0x00))
			{
				BPass = 0x01;//蜂鸣器提示成功
				BeepCount = 0x01;//蜂鸣器与LED提示1次
				return;
			}
		}
		for(i=0;i<UART_RX_BUF_LEN;i++)	//清空串口缓冲区
			UartRxBuf[i] = 0x00; 
	}
}
/*********************主函数**************************/
//MCU工作频率18.432MHZ，单片机型号为STC11F32，测试通过
void main(void)		
{
	unsigned char i;
  InitAll();
	Pass();//开机蜂鸣器提示
	BeepCount = 0x00;
	for(i=0;i<UART_RX_BUF_LEN;i++)
		UartRxBuf[i] = 0;  
	while(1)
	{	
		Key();//通过按键设置读卡器工作模式
		ProcessUartRxData(); //处理串口缓冲区数据
		if(BPass)	
		{
			BPass = 0x00;
			while(BeepCount--)
		  	Pass();	
		}
	}
}