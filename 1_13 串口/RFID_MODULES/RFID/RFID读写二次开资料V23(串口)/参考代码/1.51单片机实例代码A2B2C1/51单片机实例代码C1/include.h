#include "STC11.H"
#include "intrins.h"
#include <string.h>

#define uchar unsigned char
#define uint unsigned int


#define  RELOAD_COUNT  0xfb    //18.432M  9600  0xfb
                               //         4800  0xf6 

#define UART_RX_BUF_LEN 30		//串口接收缓冲区大小

sbit  LED1 = P3^2;
sbit  LED2 = P3^3;
sbit  LED3 = P3^4;
#define Led1_On()  LED1 = 0;
#define Led1_Off()  LED1 = 1;
#define Led2_On()  LED2 = 0;
#define Led2_Off()  LED2 = 1;
#define Led3_On()  LED3 = 0;
#define Led3_Off()  LED3 = 1;

#define KEY1   P12
#define KEY2   P13 
#define KEY3   P14  
#define KEY4   P15

#define nop() _nop_()

#define	 EnIrq()  EA = 1
#define  DisIrq() EA = 0

#define Timer1StartCount() TR1 = 1  //定时器开始计数
#define Timer1StopCount()  TR1 = 0	//定时器停止计数


#define  CLR_BEEP  P45 = 0;
#define  SET_BEEP  P45 = 1;

#define  Rs485_Tx_Mode()  P11=1;//RS485 TXD mode
#define  Rs485_Rx_Mode()  P11=0;//RS485 RXD mode


#define MIN_PKT_LEN      8   //主动读卡号的数据包长度为12字节
#define MAX_PKT_LEN      28		//主动读卡号+指定数据块的数据包长度为28字节

