from machine import UART
from time import sleep


class RFID:
    def __init__(self,port=1,baudrate=115200,rx=18,tx=19):
        self.port = UART(port,baudrate=baudrate,rx=rx,tx=tx)
        self.new_card_flag = False
        
    
    def waitMsg(self):
        dat = []
        if(self.port.any()):
            sleep(0.1)
            moreBytes = self.port.any()
            if moreBytes:
                dat = self.port.read()
                return("{:02x}{:02x}".format(dat[5],dat[6]),\
                       "{:02x}{:02x}{:02x}{:02x}".format(dat[7],dat[8],dat[9],dat[10]))
        return(False,0)