from rfid import RFID
from time import sleep

module = RFID()

while True:
    ic_type,ic_number = module.waitMsg()
    if(ic_type):
        print("Valid card{type:"+ic_type+";number:"+ic_number+"}")
    sleep(1)