from machine import Pin
from time import sleep


led = Pin(2,Pin.OUT)
switch = Pin(4,Pin.IN)


cnt = 0
max_value = 10
valve = 5
counter = 0


def toggle():
    global counter
    if(led.value() > 0):
        led.off()
    else:
        led.on()
    counter = counter + 1
    print("current number:",counter)
          

while True:
    #按钮部分
    if(switch.value()>0):
        if(cnt < max_value):
            cnt = cnt +1
    else:
        cnt = 0
    if(cnt==valve):
        toggle()
        
    sleep(0.01)