from motor import MOTOR
from machine import Timer

__all__ = ["CH"]


DOWN = True
UP   = False

class CH:
	'''
		各通道控制类

	'''
	def __init__(self,ctl_p,ctl_m,ctl_s,rtc_id,txt_id,btn1_id,btn2_id,circle_id,screen_object,tim,name="ch?",change_direct_sec=60,screen_id=1,speed=1,countdown=900):
		self.motor = MOTOR(ctl_p,ctl_m,ctl_s)
		self.rtc_id = rtc_id
		self.txt_id = txt_id
		self.stop_start_btn = btn1_id
		self.pause_resume_btn = btn2_id
		self.circle_id = circle_id
		self.screen_id = screen_id
		self.screen = screen_object
		self.surplus_time = countdown
		self.speed = speed
		self.change_direct_sec = change_direct_sec
		self.timer = Timer(tim)
		self.name = name
		#tim_ch1.init(period=180000,mode=Timer.PERIODIC,callback=changeDirection)


	def changeDirection(self,t):
		print(self.name+"change direction")
		self.motor.changeDirection()


	def init(self):
		self.screen.setButton(self.screen_id,self.stop_start_btn,0)
		self.screen.setButton(self.screen_id,self.pause_resume_btn,0)
		self.screen.stopTimer(self.screen_id,self.rtc_id)
		self.screen.setTimer(self.screen_id,self.rtc_id,self.surplus_time)


	def start(self,speed=1):
		'''
			启动通道倒计时
		'''
		self.motor.forward(self.speed)
		self.screen.setTimer(self.screen_id,self.rtc_id,self.surplus_time)
		self.screen.startTimer(self.screen_id,self.rtc_id)
		self.timer.init(period=self.change_direct_sec*1000,mode=Timer.PERIODIC,callback=self.changeDirection)


	def stop(self,speed=1):
		'''
			停止电机
			重置通道倒计时
		'''
		self.motor.stop()
		self.screen.stopTimer(self.screen_id,self.rtc_id)
		self.screen.setTimer(self.screen_id,self.rtc_id,self.surplus_time)
		self.timer.deinit()
		#self.timer.deinit(period=self.change_direct_sec*60,mode=Timer.PERIODIC,callback=self.changeDirection)


	def pause(self,speed=1):
		'''
			启动通道倒计时
		'''
		self.motor.stop()
		self.direct = self.motor.direction()
		self.screen.pauseTimer(self.screen_id,self.rtc_id)
		self.timer.deinit()


	def resume(self,speed=1):
		'''
			启动通道倒计时
		'''
		if self.direct:
			self.motor.forward(self.speed)
		else:
			self.motor.reverse(self.speed)
		self.screen.startTimer(self.screen_id,self.rtc_id)
		self.timer.init(period=self.change_direct_sec*60,mode=Timer.PERIODIC,callback=self.changeDirection)


	def setCounter(self,counter):
		self.stop()
		self.surplus_time = counter
		self.screen.setTimer(self.screen_id,self.rtc_id,self.surplus_time)


	def cycleCH(self):
		surplus_second = self.screen.getTime(self.screen_id,self.rtc_id)
		if(((surplus_second%self.change_direct_sec)==0) and surplus_second):
			self.motor.changeDirection()
		elif(surplus_second==0):
			self.motor.stop()
			self.screen.setButton(self.screen_id,self.stop_start_btn,UP)
			self.screen.setButton(self.screen_id,self.pause_resume_btn,UP)
			self.screen.setCircle(self.screen_id,self.circle_id,360)
		else:
			self.screen.setCircle(self.screen_id,self.circle_id,int(360-360*(surplus_second/self.surplus_time)))
		pass	

