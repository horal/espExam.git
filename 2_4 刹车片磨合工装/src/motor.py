from machine import Pin,PWM
from time import sleep


__all__ = ["MOTOR","forward","reverse"]


class MOTOR:
    '''
        这是基于L298N的直流电机控制类.
        L298N共可控制2个直流电机，本类使用其中的一组.
    '''
    def __init__(self,ctl_p,ctl_m,ctl_s,speed=1,hold_time=1):
        '''
            传入控制引脚初始化控制类
            ctl_p/ctl_m是控制引脚
            ctl_s为速度引脚
        '''
        self.ctl_p = Pin(ctl_p)
        self.ctl_m = Pin(ctl_m)
        self.ctl_s = PWM(Pin(ctl_s))
        self.hold_time = hold_time
        self.speed=speed
        self.ctl_s.duty(int(self.speed*1024))
        pass
    
    
    def forward(self,speed=1):
        '''
            speed速度（0~1）
            speed=0电机停机
            speed=1电机全速运k行
        '''
        self.ctl_p.value(1)
        self.ctl_m.value(0)
        self.speed=speed
        self.ctl_s.duty(int(self.speed*1024))
        pass


    def reverse(self,speed=1):
        '''
            speed速度（0~1）
            speed=0电机停机
            speed=1电机全速运k行
        '''
        self.ctl_p.value(0)
        self.ctl_m.value(1)
        self.speed=speed
        self.ctl_s.duty(int(self.speed*1024))
        pass

    
    def stop(self):
        '''
            speed速度（0~1）
            speed=0电机停机
            speed=1电机全速运k行
        '''
        self.speed=0
        self.ctl_s.duty(int(self.speed*1024))
        self.ctl_p.value(0)
        self.ctl_m.value(0)


    def changeDirection(self):
        '''
            电机转向
        '''
        if self.ctl_p.value():
            self.stop()
            sleep(self.hold_time)
            self.reverse()
        else:
            self.stop()
            sleep(self.hold_time)
            self.forward()


    def direction(self):
        '''
            speed速度（0~1）
            speed=0电机停机
            speed=1电机全速运k行
        '''
        return self.ctl_p.value()
