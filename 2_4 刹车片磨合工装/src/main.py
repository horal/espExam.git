from dc80480 import DC80480
from ch import CH
from time import sleep
import json
from machine import Timer

#通道1参数
CH1_RTC_ID = 1
CH1_TXT_ID = 21
CH1_STOP_START_BUTTON_ID = 31
CH1_PAUSE_RESUME_BUTTON_ID = 41
CH1_CIRCLE_ID = 51
CH1_CTL_P = 13
CH1_CTL_M = 12
CH1_CTL_S = 14
CH1_TIMER = 0

#通道2参数
CH2_RTC_ID = 2
CH2_TXT_ID = 22
CH2_STOP_START_BUTTON_ID = 32
CH2_PAUSE_RESUME_BUTTON_ID = 42
CH2_CIRCLE_ID = 52
CH2_CTL_P = 27
CH2_CTL_M = 26
CH2_CTL_S = 25
CH2_TIMER = 1

#通道3参数
CH3_RTC_ID = 3
CH3_TXT_ID = 23
CH3_STOP_START_BUTTON_ID = 33
CH3_PAUSE_RESUME_BUTTON_ID = 43
CH3_CIRCLE_ID = 53
CH3_CTL_P = 5
CH3_CTL_M = 4 
CH3_CTL_S = 15
CH3_TIMER = 2

#通道4参数
CH4_RTC_ID = 4
CH4_TXT_ID = 24
CH4_STOP_START_BUTTON_ID = 34
CH4_PAUSE_RESUME_BUTTON_ID = 44
CH4_CIRCLE_ID = 54
CH4_CTL_P = 21
CH4_CTL_M = 22
CH4_CTL_S = 23
CH4_TIMER = 3

#屏幕实例
screen = DC80480()

#各通道实例
ch1 = CH(CH1_CTL_P,CH1_CTL_M,CH1_CTL_S,CH1_RTC_ID,CH1_TXT_ID,CH1_STOP_START_BUTTON_ID,CH1_PAUSE_RESUME_BUTTON_ID,CH1_CIRCLE_ID,screen,CH1_TIMER,name="ch1")
ch2 = CH(CH2_CTL_P,CH2_CTL_M,CH2_CTL_S,CH2_RTC_ID,CH2_TXT_ID,CH2_STOP_START_BUTTON_ID,CH2_PAUSE_RESUME_BUTTON_ID,CH2_CIRCLE_ID,screen,CH2_TIMER,name="ch2")
ch3 = CH(CH3_CTL_P,CH3_CTL_M,CH3_CTL_S,CH3_RTC_ID,CH3_TXT_ID,CH3_STOP_START_BUTTON_ID,CH3_PAUSE_RESUME_BUTTON_ID,CH3_CIRCLE_ID,screen,CH3_TIMER,name="ch3")
ch4 = CH(CH4_CTL_P,CH4_CTL_M,CH4_CTL_S,CH4_RTC_ID,CH4_TXT_ID,CH4_STOP_START_BUTTON_ID,CH4_PAUSE_RESUME_BUTTON_ID,CH4_CIRCLE_ID,screen,CH4_TIMER,name="ch4")
chs = [ch1,ch2,ch3,ch4]
    

#按键回调函数
def buttonCallBack(msg):
    controll_id = int.from_bytes(msg[4:6],'big')
    state = msg[-1]
    #STOP_START_BUTTON
    if ((controll_id > 30) and (controll_id < 40)):
        print("button:",controll_id)
        if state:
            chs[controll_id%10-1].start()
        else:
            chs[controll_id%10-1].stop()
    #PAUSE_RESUME_BUTTON
    elif ((controll_id > 40) and (controll_id < 50)):
        if state:
            chs[controll_id%10-1].pause()
        else:
            chs[controll_id%10-1].resume()

        
#时钟调整回调函数
def rtcCallBack(msg):
    controll_id = int.from_bytes(msg[4:6],'big')
    chs[controll_id%10-1].setCounter(int(msg[7:-1]))
    screen.clearText(1,controll_id)
    #print(type(int(msg[7:-1])))

#系统初始化
for ch in chs:
    ch.init()

#主循环
while True:
    ret,msg = screen.waitMsg()
    if ret:
        if((msg[0]==0xb1) and (msg[1]==0x11) and (msg[6]==0x11)):
            rtcCallBack(msg)
        elif((msg[0]==0xb1) and (msg[1]==0x11) and (msg[6]==0x10)):
            buttonCallBack(msg)
    sleep(0.1)
