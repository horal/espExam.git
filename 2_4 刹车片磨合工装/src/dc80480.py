from machine import UART
from time import sleep


__all__ = ["DC80480","waitMsg"]


class DC80480:
    '''
        这是大彩串口屏的控制类

        串口发出的数据通过waitMsg()函数接入系统，故需要在系统中周期性调用该函数
    '''
    def __init__(self,port=1,baudrate=115200,rx=19,tx=18):
        self.port = UART(port,baudrate=baudrate,rx=rx,tx=tx)
        self.counter = 0
        
    
    def waitMsg(self):
        '''
            系统查询串口发出的数据的函数，需要在主循环中周期性被调用
        '''
        dat = []
        if(self.port.any()):
            sleep(0.1)
            moreBytes = self.port.any()
            if moreBytes:
                dat = self.port.read()
                #检测到有效命令
                if dat[0] == 0xee:
                    return(True,dat[1:-4])
        return(False,dat)


    def _write(self,cmd):
        '''
            私有函数，用于通过串口发送数据
            parameter:
                cmd:命令数组

        '''
        self.port.write(bytes(cmd))


    def clearText(self,screen_id,control_id):
        '''
            清除文本控件内容
            parameter:
                screen_id :界面ID
                control_id:控件ID
        '''
        temp = [0xee,0xb1,0x10,(screen_id&0xff00)>>8,screen_id&0xff,(control_id&0xff00)>>8,control_id&0xff,0xff,0xfc,0xff,0xff]
        self._write(temp)


    def setTimer(self,screen_id,control_id,value):
        '''
            设置RTC控件
            parameter:
                screen_id :界面ID
                control_id:控件ID
                value:配置的值
        '''
        temp = [0xee,0xb1,0x40,(screen_id&0xff00)>>8,screen_id&0xff,(control_id&0xff00)>>8,control_id&0xff,(value&0xff000000)>>24,(value&0xff0000)>>16,(value&0xff00)>>8,value&0xff,0xff,0xfc,0xff,0xff]
        self._write(temp)


    def getTime(self,screen_id,control_id):
        '''
            读取RTC控件
            parameter:
                screen_id :界面ID
                control_id:控件ID
        '''
        self.waitMsg()
        temp = [0xee,0xb1,0x45,(screen_id&0xff00)>>8,screen_id&0xff,(control_id&0xff00)>>8,control_id&0xff,0xff,0xfc,0xff,0xff]
        self._write(temp)
        while True:
            if(self.port.any()):
                sleep(0.1)
                dat = self.port.read()
                #print("surplus second:",int.from_bytes(dat[8:-4],'big'))
                return(int.from_bytes(dat[8:-4],'big'))
            sleep(0.1)


    def startTimer(self,screen_id,control_id):
        '''
            设置RTC控件
            parameter:
                screen_id :界面ID
                control_id:控件ID
        '''
        temp = [0xee,0xb1,0x41,(screen_id&0xff00)>>8,screen_id&0xff,(control_id&0xff00)>>8,control_id&0xff,0xff,0xfc,0xff,0xff]
        self._write(temp)


    def pauseTimer(self,screen_id,control_id):
        '''
            暂停RTC控件
            parameter:
                screen_id :界面ID
                control_id:控件ID
        '''
        temp = [0xee,0xb1,0x44,(screen_id&0xff00)>>8,screen_id&0xff,(control_id&0xff00)>>8,control_id&0xff,0xff,0xfc,0xff,0xff]
        self._write(temp)


    def stopTimer(self,screen_id,control_id):
        '''
            停止RTC控件
            parameter:
                screen_id :界面ID
                control_id:控件ID
        '''
        temp = [0xee,0xb1,0x42,(screen_id&0xff00)>>8,screen_id&0xff,(control_id&0xff00)>>8,control_id&0xff,0xff,0xfc,0xff,0xff]
        self._write(temp)


    def setButton(self,screen_id,control_id,state):
        '''
            停止RTC控件
            parameter:
                screen_id :界面ID
                control_id:控件ID
        '''
        if state:
            temp = [0xee,0xb1,0x10,(screen_id&0xff00)>>8,screen_id&0xff,(control_id&0xff00)>>8,control_id&0xff,0x01,0xff,0xfc,0xff,0xff]
        else:
            temp = [0xee,0xb1,0x10,(screen_id&0xff00)>>8,screen_id&0xff,(control_id&0xff00)>>8,control_id&0xff,0x00,0xff,0xfc,0xff,0xff]
        self._write(temp)


    def setCircle(self,screen_id,control_id,value):
        '''
            停止RTC控件
            parameter:
                screen_id :界面ID
                control_id:控件ID
        '''
        temp = [0xee,0xb1,0x40,(screen_id&0xff00)>>8,screen_id&0xff,(control_id&0xff00)>>8,control_id&0xff,(value&0xff000000)>>24,(value&0xff0000)>>16,(value&0xff00)>>8,value&0xff,0xff,0xfc,0xff,0xff]
        self._write(temp)


    def showControl(self,screen_id,control_id):
        '''
            显示指定的控件
            parameter:
                screen_id :界面ID
                control_id:控件ID
        '''
        temp = [0xee,0xb1,0x03,(screen_id&0xff00)>>8,screen_id&0xff,(control_id&0xff00)>>8,control_id&0xff,0x01,0xff,0xfc,0xff,0xff]
        self._write(temp)


    def hiddenControl(self,screen_id,control_id):
        '''
            隐藏指定的控件
            parameter:
                screen_id :界面ID
                control_id:控件ID
        '''
        temp = [0xee,0xb1,0x03,(screen_id&0xff00)>>8,screen_id&0xff,(control_id&0xff00)>>8,control_id&0xff,0x00,0xff,0xfc,0xff,0xff]
        self._write(temp)
