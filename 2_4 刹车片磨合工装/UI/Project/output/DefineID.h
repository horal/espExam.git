//创建画面welcome的命令索引ID
#define  _SCREEN_WELCOME                                                       0

//创建画面run的命令索引ID
#define  _SCREEN_RUN                                                           1

//创建画面set的命令索引ID
#define  _SCREEN_SET                                                           2

#define  _TXT_DIS__RUN_TEXT_DISPLAY1                                          21

#define  _BTN_RUN_BUTTON2                                                     31

//全局图片off.png
#define  _IMG_OFF_PNG                                                          2

#define  _BTN_RUN_BUTTON3                                                     36

#define  _BTN_RUN_BUTTON5                                                     33

#define  _BTN_RUN_BUTTON6                                                     34

#define  _BTN_RUN_BUTTON7                                                     35

#define  _BTN_RUN_BUTTON1                                                     41

#define  _BTN_RUN_BUTTON8                                                     46

#define  _BTN_RUN_BUTTON9                                                     42

#define  _BTN_RUN_BUTTON10                                                    43

#define  _BTN_RUN_BUTTON11                                                    44

#define  _BTN_RUN_BUTTON12                                                    45

#define  _RTC_RUN_RTC1                                                         1

#define  _RTC_RUN_RTC2                                                         2

#define  _RTC_RUN_RTC3                                                         3

#define  _RTC_RUN_RTC4                                                         4

#define  _RTC_RUN_RTC5                                                         5

#define  _RTC_RUN_RTC6                                                         6

#define  _TXT_DIS__RUN_TEXT_DISPLAY2                                          22

#define  _TXT_DIS__RUN_TEXT_DISPLAY3                                          23

#define  _TXT_DIS__RUN_TEXT_DISPLAY4                                          24

#define  _TXT_DIS__RUN_TEXT_DISPLAY5                                          25

#define  _TXT_DIS__RUN_TEXT_DISPLAY6                                          26

#define  _BTN_RUN_BUTTON4                                                     32

//画面set的背景图片
#define  _IMG_SET                                                              0

