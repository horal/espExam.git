from machine import Pin
led = Pin(2,Pin.OUT)
from time import sleep
while True:
    led.value(1-led.value())
    sleep(0.5)
    led.value(1-led.value())
    sleep(0.5)