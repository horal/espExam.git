# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
#import webrepl
#webrepl.start()
import network
from time import sleep

wlan = network.WLAN(network.STA_IF)

wlan.active(True)
wlan.disconnect()

wlan.connect("Tenda_1980B0","12345678")

while wlan.ifconfig()[0]=="0.0.0.0":
    sleep(1)
    
print(wlan.ifconfig()[0])

mac = wlan.config('mac')

print("mac->%02x:%02x:%02x:%02x:%02x:%02x" %(mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]))
