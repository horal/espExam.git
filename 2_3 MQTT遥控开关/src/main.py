from machine import Pin
from time import sleep
from umqtt.simple import MQTTClient
import machine


led = Pin(2,Pin.OUT)
for i in range(3):
    led.on()
    sleep(0.5)
    led.off()
    sleep(0.5)

ctl = Pin(27,Pin.OUT)
    
    
def msg_callback(topic,msg):
    if msg==b'on':
        led.on()
        ctl.on()
    elif msg==b'off':
        led.off()
        ctl.off()
    else:
        pass
    
unique_id = machine.unique_id()
id_str = "{:.02x}{:.02x}{:.02x}{:.02x}{:.02x}{:.02x}".format(unique_id[0],unique_id[1],unique_id[2],unique_id[3],unique_id[4],unique_id[5])
print("client_id:"+id_str+"@nvfma0")

client = MQTTClient(id_str+"@nvfma0","nvfma0.cn1.mqtt.chat",port=1883,user="admin",password="YWMtik3FjjvhEey0XQtjEv9BhT7ramIbGUiJmZE06f5xgKjDWfdAK7kR7J4RvbTqXNhLAwMAAAF84NtPXzht7EBMJIAgcN45yeo_N1lkUvPAyCd1MpYaeeVavY-5Kc_LuQ")

client.connect()
client.set_callback(msg_callback)
client.subscribe("led_ctl")


while True:
    client.wait_msg()

