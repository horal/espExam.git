from time import sleep,sleep_ms
from machine import SPI,Pin,SoftSPI

cs = Pin(4,Pin.OUT)

#max31865 = SPI(2,baudrate=100000,polarity=0,phase=1,firstbit=SPI.MSB,sck=Pin(18), mosi=Pin(23), miso=Pin(19))
max31865 = SoftSPI(baudrate=1000000,\
                   polarity=0,phase=1,\
                   firstbit=SPI.MSB,\
                   sck=Pin(18),\
                   mosi=Pin(23),\
                   miso=Pin(19))
max31865.init()

DEBUG = False

def config(value=b'\x80\xd3'):
    cs.off()
    max31865.write(value)
    cs.on()
    pass


def readConfig():
    cs.off()
    max31865.write(b'\x00')
    config = max31865.read(1)
    cs.on()
    if DEBUG:
        print(config)
    
    
def readADC():
    cs.off()
    max31865.write(b'\x01')
    msb = max31865.read(1)
    lsb = max31865.read(1)
    cs.on()
    if DEBUG:
        print("\t->msb:"+str(msb)+";lsb:"+str(lsb))
    return(msb[0],lsb[0])
    
 
def readHFault():
    cs.off()
    max31865.write(b'\x03')
    msb = max31865.read(1)
    lsb = max31865.read(1)
    cs.on()
    if DEBUG:
        print("\t->hfmsb:"+str(msb)+";hflsb:"+str(lsb))
    
  
def readLFault():
    cs.off()
    max31865.write(b'\x05')
    msb = max31865.read(1)
    lsb = max31865.read(1)
    cs.on()
    if DEBUG:
        print("\t->lfmsb:"+str(msb)+";lflsb:"+str(lsb))
    
    
def readFault():
    cs.off()
    max31865.write(b'\x07')
    config = max31865.read(1)
    cs.on()
    if DEBUG:
        print("\tfault:",config)
    
config()

while True:
    '''
        max31865手册p19,r_pt=100Ω对应0℃,
                        r_pt=111.67Ω对应30℃,带入公式
        t = r*a+b可得a = 2.57，b=-257
        ∴temperature = 2.57*r_pt - 257
    '''
    m,l = readADC()
    adc_code = (((m*256)+l)>>1)
    temp = ((adc_code/32) -256)
    r_pt = ((adc_code*430)/32768)
    print("R:",r_pt)
    print("temperature:%3.2f ℃" %(2.57*r_pt-257))
    sleep(1)
