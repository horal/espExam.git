from max31865 import MAX31865
from time import sleep

sensor = MAX31865(18,23,19,4)
    
while True:
    print("Temperature:%3.2f" %(sensor.readTemperature()))
    sleep(1)