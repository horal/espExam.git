from machine import Pin,TouchPad
from time import sleep

led = Pin(2,Pin.OUT)
tp = TouchPad(Pin(14,Pin.IN))

tp_valve = 700
cnt = 0
max_value = 10
valve = 5

def button_deal():
    if(led.value() > 0):
        led.off()
    else:
        led.on()
    print("valid btn")
    
while True:
    #按钮部分
    if(tp.read()<tp_valve):
        if(cnt < max_value):
            cnt = cnt +1
    else:
        cnt = 0
    if(cnt==valve):
        button_deal()
        
    sleep(0.01)