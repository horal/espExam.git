EESchema Schematic File Version 4
LIBS:schematic-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3600 4250 3850 4250
Wire Wire Line
	3600 4150 3850 4150
Wire Wire Line
	3600 3650 3850 3650
Wire Wire Line
	3600 3550 3850 3550
Wire Wire Line
	3600 3350 3850 3350
Wire Wire Line
	3600 3150 3850 3150
Wire Wire Line
	3600 3050 3850 3050
Text Label 3850 4350 0    50   ~ 0
D22
Text Label 3850 4450 0    50   ~ 0
D23
Text Label 3850 4250 0    50   ~ 0
TX0
Text Label 3850 4150 0    50   ~ 0
RX0
Text Label 3850 4050 0    50   ~ 0
D21
Text Label 3850 3950 0    50   ~ 0
D19
Text Label 3850 3850 0    50   ~ 0
D18
Text Label 3850 3750 0    50   ~ 0
D5
Text Label 3850 3650 0    50   ~ 0
TX2
Text Label 3850 3550 0    50   ~ 0
RX2
Text Label 3850 3450 0    50   ~ 0
D4
Text Label 3850 3350 0    50   ~ 0
D2
Text Label 3850 3250 0    50   ~ 0
D15
Text Label 3850 3150 0    50   ~ 0
GND
Text Label 3850 3050 0    50   ~ 0
3V3
$Comp
L Connector_Generic:Conn_02x15_Odd_Even ESP32_L
U 1 1 618D16D4
P 3300 3750
F 0 "ESP32_L" H 3350 4575 50  0000 C CNN
F 1 "Conn_02x15_Odd_Even" H 3350 4576 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x15_P2.54mm_Vertical_SMD" H 3300 3750 50  0001 C CNN
F 3 "~" H 3300 3750 50  0001 C CNN
	1    3300 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 618D16E0
P 4950 3300
F 0 "R10" V 4950 3250 50  0000 L CNN
F 1 "2.7K" H 5020 3255 50  0001 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4880 3300 50  0001 C CNN
F 3 "~" H 4950 3300 50  0001 C CNN
	1    4950 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 618D16E6
P 4950 3400
F 0 "R11" V 4950 3350 50  0000 L CNN
F 1 "2.7K" H 5020 3355 50  0001 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4880 3400 50  0001 C CNN
F 3 "~" H 4950 3400 50  0001 C CNN
	1    4950 3400
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 618D16EC
P 4950 3500
F 0 "R12" V 4950 3450 50  0000 L CNN
F 1 "2.7K" H 5020 3455 50  0001 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4880 3500 50  0001 C CNN
F 3 "~" H 4950 3500 50  0001 C CNN
	1    4950 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 618D16F2
P 4950 3600
F 0 "R13" V 4950 3550 50  0000 L CNN
F 1 "2.7K" H 5020 3555 50  0001 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4880 3600 50  0001 C CNN
F 3 "~" H 4950 3600 50  0001 C CNN
	1    4950 3600
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 618D16F8
P 4950 3700
F 0 "R14" V 4950 3650 50  0000 L CNN
F 1 "2.7K" H 5020 3655 50  0001 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4880 3700 50  0001 C CNN
F 3 "~" H 4950 3700 50  0001 C CNN
	1    4950 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 618D16FE
P 4950 3800
F 0 "R15" V 4950 3750 50  0000 L CNN
F 1 "2.7K" H 5020 3755 50  0001 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4880 3800 50  0001 C CNN
F 3 "~" H 4950 3800 50  0001 C CNN
	1    4950 3800
	0    1    1    0   
$EndComp
$Comp
L Device:R R16
U 1 1 618D1704
P 4950 3900
F 0 "R16" V 4950 3850 50  0000 L CNN
F 1 "2.7K" H 5020 3855 50  0001 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4880 3900 50  0001 C CNN
F 3 "~" H 4950 3900 50  0001 C CNN
	1    4950 3900
	0    1    1    0   
$EndComp
$Comp
L Device:R R17
U 1 1 618D170A
P 4950 4000
F 0 "R17" V 4950 3950 50  0000 L CNN
F 1 "2.7K" H 5020 3955 50  0001 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4880 4000 50  0001 C CNN
F 3 "~" H 4950 4000 50  0001 C CNN
	1    4950 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	5250 3300 5100 3300
Wire Wire Line
	5100 3400 5250 3400
Wire Wire Line
	5250 3500 5100 3500
Wire Wire Line
	5100 3600 5250 3600
Wire Wire Line
	5250 3700 5100 3700
Wire Wire Line
	5100 3800 5250 3800
Wire Wire Line
	5250 3900 5100 3900
Wire Wire Line
	5100 4000 5250 4000
Wire Wire Line
	4800 4000 4600 4000
Wire Wire Line
	4600 4000 4600 4450
Wire Wire Line
	3600 4450 4600 4450
Wire Wire Line
	4800 3900 4550 3900
Wire Wire Line
	4550 3900 4550 4350
Wire Wire Line
	3600 4350 4550 4350
Wire Wire Line
	4800 3800 4500 3800
Wire Wire Line
	4500 3800 4500 4050
Wire Wire Line
	3600 4050 4500 4050
Wire Wire Line
	4800 3700 4450 3700
Wire Wire Line
	4450 3700 4450 3950
Wire Wire Line
	3600 3950 4450 3950
Wire Wire Line
	4800 3600 4400 3600
Wire Wire Line
	4400 3600 4400 3850
Wire Wire Line
	3600 3850 4400 3850
Wire Wire Line
	4800 3500 4350 3500
Wire Wire Line
	4350 3500 4350 3750
Wire Wire Line
	3600 3750 4350 3750
Wire Wire Line
	4800 3400 4300 3400
Wire Wire Line
	4300 3400 4300 3450
Wire Wire Line
	3600 3450 4300 3450
Wire Wire Line
	4300 3250 4300 3300
Wire Wire Line
	4300 3300 4800 3300
Wire Wire Line
	3600 3250 4300 3250
$Comp
L Display_Character:HDSP-A153-duble U?
U 1 1 618E85D3
P 5550 3600
F 0 "U?" H 5725 4267 50  0001 C CNN
F 1 "HDSP-A153-duble" H 5725 4176 50  0001 C CNN
F 2 "Display_7Segment:HDSP-A151" H 5350 2750 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 5150 4150 50  0001 C CNN
	1    5550 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 3300 6900 3300
Text Label 6900 3300 0    50   ~ 0
D13
Wire Wire Line
	7200 3400 6900 3400
Text Label 6900 3500 0    50   ~ 0
D14
Text Label 6900 3400 0    50   ~ 0
D12
$Comp
L Connector_Generic:Conn_02x15_Odd_Even ESP32_R
U 1 1 619001C3
P 7400 3800
F 0 "ESP32_R" H 7450 4625 50  0000 C CNN
F 1 "Conn_02x15_Odd_Even" H 7450 4626 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x15_P2.54mm_Vertical_SMD" H 7400 3800 50  0001 C CNN
F 3 "~" H 7400 3800 50  0001 C CNN
	1    7400 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 3100 7050 3100
Wire Wire Line
	6900 3200 7200 3200
Wire Wire Line
	6900 3600 7200 3600
Wire Wire Line
	6900 3700 7200 3700
Wire Wire Line
	6900 4000 7200 4000
Wire Wire Line
	6900 4100 7200 4100
Wire Wire Line
	6900 4200 7200 4200
Wire Wire Line
	6900 4300 7200 4300
Wire Wire Line
	6900 4400 7200 4400
Wire Wire Line
	6900 4500 7200 4500
Text Label 6900 4500 0    50   ~ 0
EN
Text Label 6900 4400 0    50   ~ 0
VP
Text Label 6900 4300 0    50   ~ 0
VN
Text Label 6900 4200 0    50   ~ 0
D34
Text Label 6900 4100 0    50   ~ 0
D35
Text Label 6900 4000 0    50   ~ 0
D32
Text Label 6900 3900 0    50   ~ 0
D33
Text Label 6900 3800 0    50   ~ 0
D25
Text Label 6900 3700 0    50   ~ 0
D26
Text Label 6900 3600 0    50   ~ 0
D27
Wire Wire Line
	6200 3950 6550 3950
Wire Wire Line
	6550 3950 6550 3800
Wire Wire Line
	6550 3800 7200 3800
Wire Wire Line
	6200 4050 6600 4050
Wire Wire Line
	6600 4050 6600 3900
Wire Wire Line
	6600 3900 7200 3900
$Comp
L Switch:SW_Push SW?
U 1 1 619320F3
P 6650 3250
F 0 "SW?" H 6650 3535 50  0001 C CNN
F 1 "SW_Push" H 6650 3444 50  0001 C CNN
F 2 "" H 6650 3450 50  0001 C CNN
F 3 "~" H 6650 3450 50  0001 C CNN
	1    6650 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 3500 6650 3450
Wire Wire Line
	6650 3500 7200 3500
Wire Wire Line
	6650 3050 6650 2800
Text Label 6650 2800 1    50   ~ 0
3V3
$EndSCHEMATC
