from time import sleep,sleep_ms
from CADT import digitalCube2
from button import BUTTON
from machine import Pin

counter = 0
disp = digitalCube2(15,4,5,18,19,21,22,23,25,33)
led = Pin(2,Pin.OUT)

def btnDeal():
    global counter
    print("btn down")
    counter = counter +1
    disp.updateDisplay(counter)
    
def longBtnDeal():
    global counter
    led.value(1-led.value())
    counter = 0
    disp.updateDisplay(counter)
     
btn = BUTTON(12,btnDeal,longCallBack=longBtnDeal,holdCallBack=btnDeal) 

#循环计数器
i = 0
  
disp.updateDisplay(counter)

while True:
    i = i+1
    if(i>999):
        i=0
    if not (i%10):
        btn.cycle()
    disp.display()
    sleep(0.001)