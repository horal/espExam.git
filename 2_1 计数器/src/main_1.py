from time import sleep,sleep_ms
from button import BUTTON
from machine import Pin

counter = 0
led = Pin(2,Pin.OUT)

def btnDeal():
    global counter
    print("btn down")
    counter = counter +1
    disp.updateDisplay(counter)
    
def longBtnDeal():
    global counter
    led.value(1-led.value())
    counter = 0
     
btn = BUTTON(12,btnDeal,longCallBack=longBtnDeal,holdCallBack=btnDeal) 

#循环计数器
i = 0

while True:
    i = i+1
    if(i>99):
        i=0
    btn.cycle()
    sleep(0.01)
