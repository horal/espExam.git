from machine import Pin

led = Pin(2,Pin.OUT)

from time import sleep

while True:
    led.on()
    sleep(0.5)
    led.off()
    sleep(0.5)